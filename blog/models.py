from __future__ import unicode_literals

from django.db import models

from django.contrib import admin
from django.contrib.auth.models import User

from django.db import models
from django.db.models import Q
from django.conf import settings

from datetime import timedelta
from hashlib import sha1
import traceback
from StringIO import StringIO
import logging


# tabla customer (clientes)
class customer(models.Model):
    customer_id = models.IntegerField(primary_key=True)
    firstname = models.CharField(max_length=250) 
    lastname = models.CharField(max_length=250)
    email = models.CharField(max_length=100)
    telephone = models.CharField(max_length=100)
    password = models.CharField(max_length=128)
    token = models.CharField(max_length=128)
    date_added = models.DateTimeField(auto_now=False, auto_now_add=True)
    rut_empresa = models.CharField(max_length=250)
    razon_social = models.CharField(max_length=250)
    direccion = models.CharField(max_length=250, null=True)
    comuna = models.CharField(max_length=250, null=True)
    status = models.IntegerField()
    envios = models.SlugField()


class customerAdmin(admin.ModelAdmin):
    list_display = ['firstname', 'lastname', 'email', 'telephone', 'date_added', 'rut_empresa', 'razon_social','direccion','comuna']
admin.site.register(customer, customerAdmin)

#tabla de perfiles
class profile(models.Model):
    user = models.OneToOneField(User, related_name="user", db_column="user_id", primary_key=True)  
    id_empresa = models.ForeignKey(customer, related_name="empresa", db_column="id_empresa")  
    id_usercreador = models.IntegerField()
    foto = models.FileField(blank=True, null=True)
    banner = models.FileField(blank=True, null=True)
    foto_empresa = models.FileField(blank=True, null=True)

class profileAdmin(admin.ModelAdmin):
    list_display = ['user', 'get_empresa', 'id_usercreador']
	
    def get_empresa(self, obj):
        return obj.id_empresa.razon_social
    get_empresa.short_description = "empresa"
    get_empresa.admin_order_field = "id_empresa__razon_social"

admin.site.register(profile, profileAdmin)


#tabla de reportes -- sirve para guaradr los reportes de envio generados
class archivos(models.Model):
    id_archivo = models.IntegerField(primary_key=True)
    nombre = models.CharField(max_length=200)
    ruta = models.CharField(max_length=250)
    creacion = models.DateTimeField(auto_now=False, auto_now_add=False)
    tipo_archivo = models.CharField(max_length=10)
    tipo= models.IntegerField()

class archivosAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'ruta', 'creacion', 'tipo']

admin.site.register(archivos, archivosAdmin)

#tabla de compras, se genera luego de ser cargados los creditos 
class compra(models.Model):
    id_compra = models.IntegerField(primary_key=True)
    cantidad_sms = models.IntegerField()
    cantidad_mail = models.IntegerField()
    fecha = models.DateTimeField(auto_now=False, auto_now_add=False)
    valor = models.IntegerField()
    id_cuenta_corriente = models.IntegerField()
    tipo_compra = models.IntegerField() #0 : normal -- 1: khipu

class compraAdmin(admin.ModelAdmin):
    list_display = ['id_cuenta_corriente', 'cantidad_sms', 'cantidad_mail', 'fecha']

admin.site.register(compra, compraAdmin)

#tabla de conectores
class conector(models.Model):
    id_conector = models.IntegerField(primary_key=True)
    tipo =  models.CharField(max_length=20)
    nombre =  models.CharField(max_length=255)
    glosa =  models.CharField(max_length=100)
    defecto = models.IntegerField()
    ip_servidor = models.CharField(max_length=45)
    params =  models.CharField(max_length=255)

    def get_absolute_url(self):
        return reverse('conector_detail', args=[str(self.id)])

class conectorAdmin(admin.ModelAdmin):
    list_display = ['tipo', 'nombre', 'glosa']

admin.site.register(conector, conectorAdmin)

#tabla de pedidos, se genera antes de efectuar la compra
class pedido(models.Model):
    id_pedido = models.IntegerField(primary_key = True)
    id_empresa = models.IntegerField()
    cantidad_sms = models.IntegerField()
    cantidad_mail = models.IntegerField()
    token = models.CharField(max_length= 255)

#tabla de contactos
class contacto(models.Model):
    id_contacto = models.IntegerField(primary_key=True)
    id_lista = models.IntegerField()
    nombre = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    celular = models.CharField(max_length=25)
    estado = models.IntegerField()
    fecha_nac = models.DateField(auto_now=False, auto_now_add=False)
    sexo = models.CharField(max_length=25)
    desinscrito = models.IntegerField()
    custom = models.SlugField()

class contactoAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'email', 'celular']

admin.site.register(contacto, contactoAdmin)

#tabla de cuenta corriente
class cuenta_corriente(models.Model):	
    id_cuenta_corriente = models.IntegerField(primary_key=True)
    saldo_credito = models.IntegerField()
    saldo_mail = models.IntegerField()
    saldo_sms = models.IntegerField()
    consumidos_mail = models.IntegerField()
    consumidos_sms = models.IntegerField()
    id_conector_sms = models.ForeignKey(conector, related_name="conector_sms")
    id_conector_mail = models.ForeignKey(conector, related_name="conector_mail")

class cuentaAdmin(admin.ModelAdmin):
    list_display = ['id_cuenta_corriente', 'consumidos_mail', 'consumidos_sms', 'conectorsms', 'conectormail']

    def conectorsms(self, obj):
        return obj.id_conector_sms.nombre
    conectorsms.short_description = 'Conector sms'

    def conectormail(self, obj):
        return obj.id_conector_mail.nombre
    conectormail.short_description = 'Conector mail'

admin.site.register(cuenta_corriente, cuentaAdmin)

#tabla de consumo se genran registros despues de efectuar un envio
class consumo(models.Model):
    id_consumo = models.IntegerField(primary_key=True)
    sms_consumidos = models.IntegerField()
    mail_consumidos = models.IntegerField()
    fecha = models.DateTimeField(auto_now=False, auto_now_add=False)
    valor = models.IntegerField()
    cuenta_corriente = models.OneToOneField(cuenta_corriente, related_name="cuenta_corriente",  db_column="id_cuenta_corriente")

class consumoAdmin(admin.ModelAdmin):
    list_display = ['cuenta_corriente', 'id_consumo', 'sms_consumidos', 'mail_consumidos', 'fecha', 'valor']

admin.site.register(consumo, consumoAdmin)

#tabla de envio
class envio(models.Model):	
    id_envio = models.IntegerField(primary_key=True)
    estado = models.CharField(max_length=255)
    remitente = models.CharField(max_length=70)
    correo_remitente = models.CharField(max_length=100)
    id_lista = models.IntegerField()
    id_mensaje = models.IntegerField()
    tipo_envio = models.CharField(max_length=20)
    tipo_mensaje = models.CharField(max_length=20)
    cuando_enviar =  models.DateTimeField(auto_now=False, auto_now_add=False)
    nombre_envio = models.CharField(max_length=250)
    envios_malos = models.IntegerField()
    id_usuario = models.IntegerField()
    datos_envio_programado = models.CharField(max_length=255)
    id_empresa = models.IntegerField()
    id_consumo = models.IntegerField()
    failover = models.IntegerField()
    estadisticas = models.TextField()

#tabla de detalle envio
class detalle_envio(models.Model):
    id_detalle_envio = models.IntegerField(primary_key=True)
    id_respuesta_servidor = models.CharField(max_length=250)
    estado = models.CharField(max_length=100)
    id_envio = models.ForeignKey(envio,  related_name="envio", db_column="id_envio")

    empresa_telefono_receptor = models.CharField(max_length=100)
    destinatario = models.CharField(max_length=100)
    intentos = models.IntegerField()
    id_contacto = models.IntegerField()
    fecha = models.DateTimeField(auto_now=False, auto_now_add=False)
    estado_open = models.IntegerField()
    estado_click = models.IntegerField()
    estado_spam	= models.IntegerField()

#tabla de codigo de pais (por el momento sin usar ya que por el momento no se hacen envios fuera de Chile)
class codigo_pais(models.Model):
    id = models.IntegerField(primary_key=True)
    pais = models.CharField(max_length=250)
    codigo = models.IntegerField()
    abreviatura = models.CharField(max_length=2)
    activo = models.IntegerField()
	
#tabla de listas de contacto
class lista_contacto(models.Model):
    id_lista = models.IntegerField(primary_key=True)
    nombre = models.CharField(max_length=250)
    fecha_creacion = models.DateTimeField(auto_now=False, auto_now_add=False)
    ultima_actualizacion = models.DateTimeField(auto_now=False, auto_now_add=False)
    id_empresa = models.IntegerField()
    id_usuario = models.IntegerField()
    borrado = models.IntegerField()

#tabla de mensajes predefinidos, se genera un registro al guardar como predefinido 
class mensaje(models.Model):
    id_mensaje = models.IntegerField(primary_key=True)
    tipo = models.CharField(max_length=250)
    titulo = models.CharField(max_length=250)
    cuerpo = models.TextField()
    ruta_archivo_adjunto = models.CharField(max_length=250)
    remitente = models.CharField(max_length=50)
    correo_remitente = models.CharField(max_length=250)
    is_predefinido = models.BooleanField()
    fecha_creacion = models.DateTimeField(auto_now=False, auto_now_add=False)
    id_usuario = models.IntegerField()
    id_empresa = models.IntegerField()
    borrado = models.IntegerField()
    nombre_predefinido = models.CharField(max_length=250)
    nombre_envio = models.CharField(max_length=250)

    def __unicode__(self):
        return self.cuerpo

#tabla de sms predefinido, se genera un registro al enviar un sms de respuesta al celular 
class sms_recibido(models.Model):
    id_recibido = models.IntegerField(primary_key=True)
    mensaje = models.CharField(max_length=500)
    fecha = models.DateTimeField(auto_now=False, auto_now_add=False)
    id_respuesta_servidor = models.CharField(max_length=250)
    id_empresa = models.ForeignKey(customer, related_name="empresasms", db_column="id_empresa" ) 
    destinatario =  models.CharField(max_length=250)
    remitente = models.CharField(max_length=250)
    borrado =  models.IntegerField()

#tabla de estados, tipifica  los estados de sms y mail  
class estadoenvio(models.Model):
    id_estadoenvio = models.IntegerField(primary_key=True)
    estado_raw = models.CharField(max_length=255)
    id_estadohum = models.IntegerField()
    detalle_estado = models.CharField(max_length=250)
    estado_vista = models.CharField(max_length=255) 

#taba de numeracion, en desuso al momento de hacer la integracion
class tabla_numeracion(models.Model):
    id_numeracion = models.IntegerField(primary_key=True)
    rango = models.IntegerField()
    compania = models.CharField(max_length=250)
    digitos_rango = models.IntegerField()
	
#tabla de campo, se genera al guardar una lista con tipos y posicion de campos
class campo(models.Model):
    id_campo = models.IntegerField(primary_key=True)
    campo = models.CharField(max_length=250)
    id_lista = models.IntegerField() 
    glosa = models.CharField(max_length=250)
    posicion = models.IntegerField()

#tabla de plantillas de mail, que sirven de comunicacion entre el administrador y el cliente
class templatemsg(models.Model):
    id_templatemsg = models.IntegerField(primary_key=True)
    asunto = models.CharField(max_length=255)
    cuerpo = models.TextField()

#tabla de archivos subidos, guardan las imagenes y archivos subidos por los clientes a la plataforma
class uploads(models.Model):
    id_uploads = models.IntegerField(primary_key=True)
    id_empresa = models.IntegerField()
    nombre_archivo = models.CharField(max_length=255)
    ruta_archivo = models.CharField(max_length=255)
    fecha_subida = models.DateTimeField(auto_now=True)

#tabla de notificaciones, se trunca cada hora
class msjinterno(models.Model):
    id_msjinterno = models.IntegerField(primary_key = True)
    id_user = models.IntegerField()
    leido = models.BooleanField()
    tipo = models.IntegerField()
    texto = models.CharField(max_length=255)

#tabla de reseller, responsable de la srelaciones entre empresas reseller de las que no
class reseller(models.Model):
    id_reseller = models.IntegerField(primary_key = True) #id de empresa
    id_empresa_padre = models.IntegerField()

#tabla de valores, parametriza el coste de los sms y mail determinado por el rango minimo y maximo
class valores(models.Model):
    id_valores = models.IntegerField(primary_key = True)
    rango_min = models.IntegerField()
    rango_max = models.IntegerField()
    valor_unitario = models.FloatField()
    tipo = models.IntegerField() #1 : SMS, 2:Mail

#tabla de lista degra de correos, determina con que correos no pueden registrarse los usuarios
class blacklist(models.Model):
    id_email = models.IntegerField(primary_key = True)
    email = models.CharField(max_length = 255)

#tabla de encolado, guarda momentaneamente la lista de mail para cargar los contactos
class encolado(models.Model):
    id_encolado = models.IntegerField(primary_key = True)
    id_envio = models.IntegerField()
    id_lista = models.IntegerField()
    id_user = models.IntegerField()
    contador = models.IntegerField()

#tabla de bugs, sirve para retroalimentacion de plataforma
class bug(models.Model):
    id_bug = models.IntegerField(primary_key = True)
    titulo = models.CharField(max_length = 255)
    descripcion = models.SlugField()
    respuesta = models.SlugField()
    customer_id = models.IntegerField()

#tabla de companias de telefonia, para integracion posterior
class integracion(models.Model):
    id_integracion =  models.IntegerField(primary_key = True)
    compania = models.CharField(max_length=255)
