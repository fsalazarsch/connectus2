from django import forms
from blog.models import customer, codigo_pais, valores


class Formregistro(forms.Form):
	username = forms.CharField(max_length= 100, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Usuario'}))
	nombre = forms.CharField(max_length= 100, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Nombre'}))
	apellidos = forms.CharField(max_length= 100, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Apellidos'}))
	fono = forms.IntegerField(widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Telefono'}))
	email = forms.EmailField(max_length= 100, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Email'}))
	password = forms.CharField(max_length= 100, min_length=8, widget=forms.PasswordInput(attrs={"class" : "form-control input-md", 'placeholder': 'Password'}))
	rut = forms.CharField(max_length= 100, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Rut'}))
	razon_social = forms.CharField(required =False, max_length= 100, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Razon Social'}))
        reseller = forms.BooleanField(required=False)

class Formguardarcust(forms.Form):
	username = forms.CharField(max_length= 100, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Usuario'}))
	rut = forms.CharField(max_length= 100, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Rut Empresa'}))
	fono = forms.IntegerField(widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Telefono'}))
	email = forms.EmailField(max_length= 100, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Email'}))
	direccion = forms.CharField(max_length= 100, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Direccion'}))
	comuna = forms.CharField(max_length= 100, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Comuna'}))

class Formimportarlista(forms.Form):
	nombre_lista = forms.CharField(required=True, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Seleccione Lista'}))
	enc = forms.BooleanField(required=False, initial= True)
	dupl = forms.BooleanField(required=False, initial = False)
	archivo = forms.FileField(required=True)

class Formguardarlista(forms.Form):
	nombre_lista = forms.CharField(required=True, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Seleccione Lista'}))
	header = forms.CharField()
	header2 = forms.CharField()
	sheet = forms.SlugField(required=False, widget=forms.Textarea(attrs={"cols" : "90"}))


class Formsms(forms.Form):
	options = forms.ChoiceField(required=False, widget=forms.RadioSelect(), choices=(("unico", "Unico"), ("masivo", "Masivo")) )
	lista_pais = forms.ChoiceField(required=False, widget=forms.Select(attrs={"class" : "form-control", "disabled":"true"}), choices= codigo_pais.objects.values_list("codigo","pais") )
	numero = forms.IntegerField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'type' : 'number', 'min' : 1000000, 'max' : 99999999}))
	numero_final = forms.IntegerField(required=False, widget=forms.HiddenInput())
	check_tipo_mensaje = forms.ChoiceField(required=False, widget=forms.RadioSelect(), choices=(("nuevo", "Nuevo"), ("predefinido", "Predefinido")) )
	titulo = forms.CharField(required=True, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder' : 'Titulo Envio'}))
	mensaje_a_enviar = forms.CharField(required=True, max_length=160, widget=forms.Textarea(attrs={"class" : "form-control input-md", 'placeholder' : 'Ingresa aqui tu mensaje'}))
	check_guardar_predefinido = forms.BooleanField(required=False, initial = False)
	nombre_predefinido = forms.CharField(required=False, max_length=100, widget=forms.TextInput(attrs={"class" : "form-control input-md"}))
	remitente = forms.CharField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md"}))
	tipo_envio =  forms.ChoiceField(required=False, widget=forms.RadioSelect(), choices=(("ahora", "Ahora"), ("programado", "Programado")) )

	hora_envio =  forms.TimeField(required=False, widget=forms.TimeInput(format=('%H:%m'), attrs={'class':'form-control', 'placeholder':'Selecciona una hora ', 'type' : 'time'}))
	fecha_envio =  forms.DateField(required=False, widget=forms.DateInput(format=('%d-%m-%Y'), attrs={'class':'form-control', 'placeholder':'Selecciona una fecha', 'type' : 'date'}))

class Formmail(forms.Form):

	options = forms.ChoiceField(required=False, widget=forms.RadioSelect(), choices=(("unico", "Unico"), ("masivo", "Masivo")) )
	lista_pais = forms.ChoiceField(required=False, widget=forms.Select(attrs={"class" : "form-control"}), choices= codigo_pais.objects.values_list("codigo","pais") )
	destinatario = forms.CharField(required=False, max_length=100, widget=forms.TextInput(attrs={"class" : "form-control input-md", "type":"mail", "placeholder" : "Ingrese el destinatario"}))
	check_tipo_mensaje = forms.ChoiceField(required=False, widget=forms.RadioSelect(), choices=(("nuevo", "Nuevo"), ("predefinido", "Predefinido")) )
	titulo = forms.CharField(required=True, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Nombre envio'}))
	asunto = forms.CharField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Asunto'}))
	cuerpo = forms.CharField(required=True, widget=forms.Textarea(attrs={"class" : "form-control input-md", 'placeholder' : 'Ingresa aqui tu mensaje', "cols" : "90"}))
	check_guardar_predefinido = forms.BooleanField(required=False, initial = False)
	nombre_predefinido = forms.CharField(required=False, max_length=100, widget=forms.TextInput(attrs={"class" : "form-control input-md"}))
	remitente = forms.CharField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md"}))
	tipo_envio =  forms.ChoiceField(required=False, widget=forms.RadioSelect(), choices=(("ahora", "Ahora"), ("programado", "Programado")) )

	hora_envio =  forms.TimeField(required=False, widget=forms.TimeInput(format=('%H:%m'), attrs={'class':'form-control', 'placeholder':'Selecciona una hora ', 'type' : 'time'}))
	fecha_envio =  forms.DateField(required=False, widget=forms.DateInput(format=('%d-%m-%Y'), attrs={'class':'form-control', 'placeholder':'Selecciona una fecha', 'type' : 'date'}))


class Formcarga(forms.Form):
	empresa = forms.IntegerField(widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Empresa'}))
	mail = forms.IntegerField(widget=forms.TextInput(attrs={"default":'0', "class" : "form-control input-md", 'placeholder': 'Email', 'type' : 'number'}))
	sms = forms.IntegerField(widget=forms.TextInput(attrs={"default":'0', "class" : "form-control input-md",  'placeholder': 'Sms', 'type' : 'number'}))

class Formuser(forms.Form):
	username = forms.CharField(max_length= 100, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Nombre de Usuario'}))
	first_name = forms.CharField(max_length= 100, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Nombre'}))
	last_name = forms.CharField(max_length= 100, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Apellidos'}))
	email = forms.EmailField(max_length= 100, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Email'}))
	password = forms.CharField(required=False, max_length= 100, widget=forms.PasswordInput(attrs={"class" : "form-control input-md", 'placeholder': 'Password'}))
	empresa = forms.ModelChoiceField(required=False, queryset=customer.objects.order_by('razon_social'), empty_label = 0, to_field_name = "customer_id")
	is_active = forms.BooleanField(required=False)

class Formprofile(forms.Form):
	id_usuario = forms.IntegerField(widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Empresa'}))
	foto = forms.FileField(required=False)
	foto_banner = forms.FileField(required=False)
	foto_empresa = forms.FileField(required=False)

class Formcontacto(forms.Form):
	CHOICES = (
    ('--', '--'),
    ('M', 'M'),
    ('F', 'F'),)

	id_contacto =  forms.IntegerField(widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'id'}))
	nombre = forms.CharField(required=False, max_length= 100, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Nombre'}))
	celular = forms.CharField(required=False, max_length= 100, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Celular'}))
	email = forms.EmailField(required=False, max_length= 100, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Email'}))
	sexo = forms.ChoiceField(required=False, choices=CHOICES)
	fecha_nac = forms.DateField(required=False, widget=forms.DateInput(attrs={'class':'form-control', 'placeholder':'Selecciona una fecha', 'type' : 'date'}))
	custom = forms.SlugField(required=False, widget=forms.Textarea(attrs={'style' : 'display:none', "cols" : "90"}))

class Formupload(forms.Form):
	nombre_archivo = forms.CharField(widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Nombre Archivo'}))
	archivo = forms.FileField()

class Formcompra(forms.Form):
        empresa = forms.IntegerField(required = True, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Empresa'}))
        mail = forms.IntegerField(widget=forms.TextInput(attrs={"value":0, "class" : "form-control input-md", "min":0, 'type': 'number'}))
        sms = forms.IntegerField(widget=forms.TextInput(attrs={"value":0, "class" : "form-control input-md", "min":0, 'type':'number'}))
        
#        sms = forms.IntegerField(widget=forms.TextInput(attrs={"value":0, "class" : "form-control input-md", "min":0,  'type': 'number'}))

class Formtemplate(forms.Form):
    id_templatemsg = forms.IntegerField(widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'id_template'}))
    titulo = forms.CharField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Asunto'}))
    cuerpo = forms.CharField(required=True, widget=forms.Textarea(attrs={"class" : "form-control input-md", 'placeholder' : 'Ingresa aqui tu mensaje', 'cols': '90'}))

class Formblacklist(forms.Form):
    titulo = forms.EmailField(required=False, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Email'}))

class Formestado(forms.Form):
	estado_raw = forms.CharField(max_length= 100, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Estado raw'}))
	detalle_estado = forms.CharField(max_length= 100, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Detalle estado'}))
	estado_vista = forms.CharField(max_length= 100, widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'Vista Estado'}))
	id_estadohum = forms.IntegerField(widget=forms.TextInput(attrs={"class" : "form-control input-md", 'placeholder': 'id_template', 'type':'number'}))

class Formbug(forms.Form):
	titulo = forms.CharField(max_length= 100, widget=forms.TextInput(attrs={"class" : "form-control input-md", "disabled":"true"}))
	descripcion = forms.CharField(required=True, widget=forms.Textarea(attrs={"class" : "form-control input-md", 'placeholder' : 'Ingresa aqui', "cols" : "90"}))
	respuesta =  forms.CharField(required=True, widget=forms.Textarea(attrs={"class" : "form-control input-md", 'placeholder' : 'Ingresa  aqui', "cols" : "90"}))
 
