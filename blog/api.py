import os, md5, hashlib, string, StringIO, json, requests, time, sendgrid, hmac
from django.shortcuts import render
from django.http import HttpResponse
from rest_framework.decorators import api_view
from rest_framework.response import Response
from blog.models import archivos, customer, conector, profile, customerAdmin, cuenta_corriente, lista_contacto, contacto, codigo_pais, mensaje, envio, compra, detalle_envio, sms_recibido, estadoenvio, tabla_numeracion, consumo, campo, templatemsg
from django.contrib.auth.models import User
from blog.serializers import UserSerializer
import hashlib
from django.contrib.auth.hashers import make_password
from rest_framework.decorators import parser_classes
from datetime import datetime, date, timedelta
from blog.views import enviar_sms, escribir_log
from django.utils.datastructures import MultiValueDictKeyError

import xmlrpclib
from SimpleXMLRPCServer import SimpleXMLRPCServer

#funcion api que envia mail
@api_view(['POST'])
def send_email(request):
    post = None
    if request.method == 'POST':

        username = request.data['username']
        passwd = request.data["password"]
        nombre_remitente = request.data["nombre_remitente"]
        email_remitente = request.data["email_remitente"]
        destinatario = request.data["destinatario"]
        asunto = request.data["asunto"]

        mje = request.data["mensaje"]

        if passwd == "" or passwd == None:
            return Response({'response': "040", 'campo' : 'passwd'})
        if username == "" or username == None:
            return Response({'response': "040", 'campo' : 'username'})
        if nombre_remitente == "" or nombre_remitente == None:
            return Response({'response': "040", 'campo' : 'nombre remitente'})
        if email_remitente == "" or email_remitente == None:
            return Response({'response': "040", 'campo' : 'email remitente'})
        if destinatario == "" or destinatario == None:
            return Response({'response': "040", 'campo' : 'destinatario'})
        if asunto == "" or asunto == None:
            return Response({'response': "040", 'campo' : 'asunto'})
        if mje == "" or mje == None:
            return Response({'response': "040", 'campo' : 'mensaje'})
        

        p = User.objects.get(username=username)
        if p.check_password(passwd):
            post = User.objects.get(username=username)
            iemp = profile.objects.get(pk= post.id).id_empresa
            salmail = cuenta_corriente.objects.values_list('saldo_mail', flat=True).get(pk = iemp.customer_id)
            if salmail == 0:
                return Response({'response': "044"})
            else:
                idcc = cuenta_corriente.objects.get(pk = iemp.customer_id)
                consmail = cuenta_corriente.objects.values_list('consumidos_mail', flat=True).get(pk = iemp.customer_id)
                cc = cuenta_corriente.objects.get(pk = iemp.customer_id)
                cc.saldo_mail = int(salmail)- 1
                cc.consumidos_mail= int(consmail)+ 1
                cc.save()

                cons = consumo(mail_consumidos= 1, fecha= datetime.now(), valor= 1, cuenta_corriente=idcc)
                cons.save()
                consid = consumo.objects.values_list('id_consumo', flat=True).latest('id_consumo')

                pre = mensaje(tipo = "MAIL", titulo = asunto, cuerpo = mje, remitente = email_remitente, is_predefinido = 0, fecha_creacion = datetime.now(), id_usuario = p.id, id_empresa = iemp.customer_id, borrado = 0, nombre_predefinido = "Enviado por API", nombre_envio = "Enviado por API")
                pre.save()
                presid = mensaje.objects.values_list('id_mensaje', flat=True).latest('id_mensaje')
                #PRESID ES LO QUE HAY QUE DEVOLVER
                d = {}
                d["destinatario"] = request.POST.get('input_email')
                d["mensaje_a_enviar"] = mje
                d["nombre_remitente"] = nombre_remitente
                d["remitente"] = email_remitente
                d["destinatario"] = destinatario
                d["asunto"] = asunto
                d["connectusKey"] = "connectusKey"
                d["id_empresa"] = str(iemp.customer_id)
                d["id_usuario"] = str(p.id)

                client = sendgrid.SendGridClient("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")
                message = sendgrid.Mail()


    
                env = envio(estado = "terminado", correo_remitente = email_remitente, id_lista = 0, id_mensaje = int(presid), tipo_envio = "API", tipo_mensaje = "MAIL", cuando_enviar =  datetime.now(), nombre_envio = asunto, id_usuario = p.id, datos_envio_programado = json.dumps(d), id_consumo = int(consid), id_empresa = iemp.customer_id, failover=0)
                env.save()
                envid = envio.objects.latest('id_envio')
                

                categoria = str(p.id)+"-"+str(envid.id_envio)+"-"+str((detalle_envio.objects.latest('id_detalle_envio').id_detalle_envio)+1)

                message.add_unique_arg("ID_EMPRESA", p.id)
                message.add_unique_arg("ID_ENVIO", envid.id_envio)
                message.add_unique_arg("DETALLE_ENVIO", (int)(detalle_envio.objects.latest('id_detalle_envio').id_detalle_envio)+1)
                message.add_category(categoria)

                message.add_to(destinatario)
                message.set_from(email_remitente)
                message.set_subject(asunto)


                header = '<p>Para asegurar la entrega de nuestros e-mail en su correo, por favor agregue '+str(email_remitente)+' a su libreta de direcciones<br>Si usted no visualiza bien este mail, haga click <a target="_blank" href="http://35.160.72.247/viewmail/0/'+str(envid.id_envio)+'/">aqui</a></p>'
                footer = '<p>Este correo electronico fue enviado a '+str(destinatario)+'</p>'

                mje = u' '.join((header, mje, footer)).encode('utf-8').strip()

                message.set_html(mje)

                resp = client.send(message)



                de = detalle_envio(id_envio = envid, id_respuesta_servidor= categoria, estado = "", destinatario = email_remitente, id_contacto = 0, fecha = datetime.now(), intentos=1, estado_open=0, estado_click=0, estado_spam =0)

                de.save()


                escribir_log("MAIL", "envio por API", "usuario: "+str(p.id)+", id_mensaje: "+str(presid)+" id_envio: "+str(envid)+", destinario :"+str(email_remitente), "ok")
                return Response({'response': "101", "id_mensaje" : str(presid) })


        else:
            return Response({'response': "010"})

        serializer = UserSerializer(post)
        return Response({'response': 40})


#funcion api que envia sms
@api_view(['POST'])
def send_sms(request):
    post = None
    if request.method == 'POST':

        try:
            username = request.data["username"]
            passwd = request.data["password"]
            numero = request.data["numero"]
            mje = request.data["mensaje"]
        except MultiValueDictKeyError:
            username = False
            passwd = False
            numero = False
            mje = False

        if passwd == "" or passwd == None:
            return Response({'response': "040", 'campo': "passwd"})
        if username == "" or username == None:
            return Response({'response': "040", 'campo': "username"})
        if numero == "" or numero == None:
            return Response({'response': "040", 'campo': "numero"})
        if mje == "" or mje == None:
            return Response({'response': "040", 'campo': "mensaje"})
        

        p = User.objects.get(username=username)
        if p.check_password(passwd):
            post = User.objects.get(username=username)
            iemp = profile.objects.get(pk= post.id).id_empresa.customer_id
            salsms = cuenta_corriente.objects.values_list('saldo_sms', flat=True).get(pk = iemp)
            if salsms == 0:
                return Response({'response': "044"})
            else:
                idcc = cuenta_corriente.objects.get(pk = iemp)
                conssms = cuenta_corriente.objects.values_list('consumidos_sms', flat=True).get(pk = iemp)
                cc = cuenta_corriente.objects.get(pk = iemp)
                cc.saldo_sms = int(salsms)- 1
                cc.consumidos_sms= int(conssms)+ 1
                cc.save()

                cons = consumo(sms_consumidos= 1, fecha= datetime.now(), valor= 1, cuenta_corriente=idcc)
                cons.save()
                consid = consumo.objects.values_list('id_consumo', flat=True).latest('id_consumo')

                pre = mensaje(tipo="SMS", cuerpo = mje.encode('utf-8'), remitente = int(numero), is_predefinido = 0, fecha_creacion = datetime.now(), id_usuario = p.id, id_empresa = iemp, borrado = 0, nombre_predefinido = "Enviado por API", nombre_envio = "Enviado por API")
                pre.save()
                presid = mensaje.objects.values_list('id_mensaje', flat=True).latest('id_mensaje')
                d = {}
                d["destinatario"] = numero
                d["mensaje_a_enviar"] = mje
                d["remitente"] = numero
                d["connectusKey"] = "connectusKey"
                d["id_empresa"] = str(iemp)
                d["id_usuario"] = str(p.id)
                
                idconn = 1
                
                env = envio(estado = "en proceso", remitente = numero, id_lista = 0, id_mensaje = presid, tipo_envio = "API", tipo_mensaje = "SMS", cuando_enviar =  datetime.now(), nombre_envio = str(datetime.now()), id_usuario = p.id, datos_envio_programado = json.dumps(d), id_consumo = consid, id_empresa = iemp, failover = idconn)
                env.save()
                envid = envio.objects.latest('id_envio')
                
                comp = tabla_numeracion.objects.values_list('compania', flat=True).filter(rango= numero[:4])

                de = detalle_envio(id_envio = envid, empresa_telefono_receptor = comp[0].encode('utf-8'), destinatario = numero, id_contacto = numero, fecha = datetime.now(), intentos=1, estado_open=0, estado_click=0, estado_spam =0)
                de.save()
                conector = cc.id_conector_sms_id
                did = detalle_envio.objects.latest('id_detalle_envio')
                if len(numero) < 9:
                    num  = '569'+str(request.POST.get('numero'))
                else:
                    num = str(request.POST.get('numero'))
                enviar_sms(conector, str(num), mje, envid.id_envio, "unico")


                escribir_log("SMS", "envio por API", "usuario: "+str(p.id)+", id_envio: "+ str(envid)+", empresa_telefono_receptor: "+ str(comp[0].encode('utf-8'))+", destinatario: "+str(numero)+", id_contacto: "+ str(numero)+", fecha: "+str(datetime.now()), "ok")
                return Response({'response': "101", "id_mensaje" : str(did.id_detalle_envio) })


        else:
            return Response({'response': "010"})

        serializer = UserSerializer(post)
        return Response({'response': 40})

#funcion sms de stark sms
@api_view(['POST'])
def send_sms_snd(request, format=None):
    post = None
    if request.method == 'POST':

        username = request.data['username']
        passwd = request.data["password"]
        numero = request.data["numero"]
        mje = request.data["mensaje"]
        sender_id = request.data["sender_id"]

        if passwd == "" or passwd == None:
            return Response({'response': "040"})
        if username == "" or username == None:
            return Response({'response': "040"})
        if numero == "" or numero == None:
            return Response({'response': "040"})
        if mensaje == "" or mensaje == None:
            return Response({'response': "040"})
        if sender_id == "" or sender_id =="None":
            return Response({'response' : "040"})

        p = User.objects.get(username=username)
        if p.check_password(passwd):
            post = User.objects.get(username=username)
            iemp = profile.objects.get(pk= post.id).id_empresa
            salsms = cuenta_corriente.objects.values_list('saldo_sms', flat=True).get(pk = iemp.customer_id)
            if salsms == 0:
                return Response({'response': "044"})
            else:
                idcc = cuenta_corriente.objects.get(pk = iemp.customer_id)
                conssms = cuenta_corriente.objects.values_list('consumidos_sms', flat=True).get(pk = iemp.customer_id)
                cc = cuenta_corriente.objects.get(pk = iemp.customer_id)
                cc.saldo_sms = int(salsms)- 1
                cc.consumidos_sms= int(conssms)+ 1
                cc.save()

                cons = consumo(sms_consumidos= 1, fecha= datetime.now(), valor= 1, cuenta_corriente=idcc)
                cons.save()
                consid = consumo.objects.values_list('id_consumo', flat=True).latest('id_consumo')

                pre = mensaje(tipo = "SMS", cuerpo = mje, remitente = numero, is_predefinido = 0, fecha_creacion = datetime.now(), id_usuario = p.id, id_empresa = iemp.customer_id, borrado = 0, nombre_predefinido = "Enviado por API", nombre_envio = "Enviado por API")
                pre.save()
                presid = mensaje.objects.values_list('id_mensaje', flat=True).latest('id_mensaje')
                #PRESID ES LO QUE HAY QUE DEVOLVER
                d = {}
                d["destinatario"] = numero

                d["mensaje_a_enviar"] = mje
                d["remitente"] = numero
                d["connectusKey"] = "connectusKey"
                d["id_empresa"] = iemp.customer_id
                d["id_usuario"] = p.id
                
                dat_env = json.dumps(d, ensure_ascii=False)
                idconn = 1

                env = envio(estado = "en proceso", remitente = numero, id_lista = 0, id_mensaje = presid, tipo_envio = "API", tipo_mensaje = "SMS", cuando_enviar =  datetime.now(), nombre_envio = str(datetime.now()), id_usuario = p.id, datos_envio_programado = json.dumps(d), id_consumo = consid, id_empresa = iemp.customer_id, failover = idconn)
                env.save()
                envid = envio.objects.latest('id_envio')
                
                comp = tabla_numeracion.objects.values_list('compania', flat=True).filter(rango= numero[:4])
                de = detalle_envio(id_envio = envid, empresa_telefono_receptor = comp[0].encode('utf-8'), destinatario = numero, id_contacto = numero, fecha = datetime.now(), intentos=0, estado_open=0, estado_click=0, estado_spam =0)
                de.save()
                
                if len(request.POST.get('numero')) < 9:
                    numero = '569'+str(request.POST.get('numero'))
                enviar_sms(conector, str(numero), request.POST.get('mensaje_a_enviar'), envid.id_envio, "unico")

                return Response({'response': "101", "sender_id" : str(sender_id) })

        else:
            return Response({'response': "010"})

        serializer = UserSerializer(post)
        return Response({'response': 40})

#funcion que retorna  el estado de sms o mail
@api_view(['GET'])
def get_status_msj_sent(request, format=None):
    post = None
    if request.method == 'GET':

        id_mensaje = request.query_params['id_mensaje']
        username = request.query_params['username']
        passwd = request.query_params["password"]
        
        if passwd == "" or passwd == None:
            return Response({'response': "040"})
        if username == "" or username == None:
            return Response({'response': "040"})
        if id_mensaje == "" or id_mensaje == None:
            return Response({'response': "040"})
        
        p = User.objects.get(username=username)
        if p.check_password(passwd):
            
            try:
                env = detalle_envio.objects.values_list('estado', flat=True).get(pk = id_mensaje)
                return Response({'response': "101", 'id_mensaje': str(id_mensaje), 'estado' : str(env) })            

            except envio.DoesNotExist:
                return Response({'response': "046"})
        else:
            return Response({'response': "010"})

#funcion que retorna creditos
@api_view(['GET'])
def get_credits(request, format=None):
    post = None
    if request.method == 'GET':

        username = request.query_params['username']
        passwd = request.query_params["password"]
        
        if passwd == "" or passwd == None:
            return Response({'response': "040"})
        if username == "" or username == None:
            return Response({'response': "040"})
        
        p = User.objects.get(username=username)
        if p.check_password(passwd):
            post = User.objects.get(username=username)
            iemp = profile.objects.get(pk= post.id).id_empresa.customer_id
            salmail = cuenta_corriente.objects.values_list('saldo_mail', flat=True).get(pk = iemp)
            salsms = cuenta_corriente.objects.values_list('saldo_sms', flat=True).get(pk = iemp)
            return Response({'response': "101", 'sms' : str(salsms), 'email' : str(salmail) })            
        else:
            return Response({'response': "010"})

