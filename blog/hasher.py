from django.contrib.auth.hashers import UnsaltedMD5PasswordHasher

from django.utils.crypto import constant_time_compare
 

class WorkingUnsaltedMD5PasswordHasher(UnsaltedMD5PasswordHasher):

    algorithm = "working_unsalted_md5"

 

    def verify(self, password, encoded):

        encoded_2 = self.encode(password, '')

        return constant_time_compare(encoded[22:], encoded_2)
