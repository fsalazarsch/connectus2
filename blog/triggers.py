﻿# -*- coding: utf-8 -*-
import os, md5, string, pyexcel, StringIO, json, requests, time, sendgrid
from django.shortcuts import render
from datetime import datetime, date, timedelta
from django.template import loader, Context
from django.http import HttpResponse, HttpResponseRedirect, FileResponse
from blog.models import archivos, customer, conector, profile, customerAdmin, cuenta_corriente, lista_contacto, contacto, codigo_pais, mensaje, envio, compra, detalle_envio, sms_recibido, estadoenvio, tabla_numeracion, consumo, campo, templatemsg

from django.contrib.auth.models import User

from forms import Formregistro, Formguardarcust, Formimportarlista, Formsms, Formmail, Formcarga, Formuser, Formguardarlista, Formprofile, Formcontacto

from django.shortcuts import render_to_response
from django.template import RequestContext

from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.hashers import make_password

from django.forms.widgets import PasswordInput, TextInput

from django.core.paginator import Paginator
from django.core.urlresolvers import reverse, reverse_lazy
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render
from django.db.models import Sum, Max

import xmlrpclib
from SimpleXMLRPCServer import SimpleXMLRPCServer

from django.conf import settings

# descripcion: Obtiene las estadisticas de todos los envios 
# input: Ninguno
# output: Ninguno
# flujo: guarda los resutados cono json en customer.envio

def estadisticas_estado():
    pass

    estadosok = estadoenvio.objects.values('estado_raw').filter(id_estadohum = 3)
    estadoserror = estadoenvio.objects.values('estado_raw').filter(id_estadohum = 1) | estadoenvio.objects.values('estado_raw').filter(id_estadohum = 4)
    rebote = estadoenvio.objects.values('estado_raw').filter(id_estadohum = 2)
    estadosespera = estadoenvio.objects.values('estado_raw').filter(id_estadohum = 0) 

    env = envio.objects.all()
    for e in env:
        if e.estadisticas == None or e.estadisticas == "" or e.estado != 'terminado':
        #crear json mail 
            if e.tipo_mensaje == "MAIL":
                det= detalle_envio.objects.filter(id_envio = e.id_envio)
                de = {}
                de["Volumen"] = det.count()
                de["Entregados"] = det.filter(estado__in= estadosok).count()
                de["Malos"] = det.filter(estado__in= estadoserror).count()
                de["Rebotes"] = det.filter(estado__in= rebote).count()
                de["Esperando"] = de["Volumen"] - ( de["Entregados"] + de["Malos"] + de["Rebotes"])
                e.estadisticas = json.dumps(de, ensure_ascii=False)
                e.save()

            if e.tipo_mensaje == "SMS":
                det = detalle_envio.objects.filter(id_envio = e.id_envio)
                carriers = det.values_list('empresa_telefono_receptor', flat=True).distinct()
                #crear json mail     
                de = {}
                de["Volumen"] = {}
                de["Esperando"] = {}
                de["Confirmados"] = {}
                de["No entregados"] = {}
                de["Confirmados"]['Total'] = det.filter(estado__in= estadosok).count()
                de["No entregados"]['Total'] = det.filter(estado__in= estadoserror).count() + det.filter(estado__in= rebote).count()
                de["Volumen"]['Total'] = det.count()
                de["Esperando"]['Total'] =  int(de["Volumen"]['Total']) - int(de["Confirmados"]['Total']) - int(de["No entregados"]['Total'])

                for carr in carriers:
                    det = detalle_envio.objects.filter(id_envio = e.id_envio, empresa_telefono_receptor = carr)			
                    de["Confirmados"][carr] = det.filter(estado__in= estadosok).count()
                    de["No entregados"][carr] = det.filter(estado__in= estadoserror).count() + det.filter(estado__in= rebote).count()
                    de["Volumen"][carr] = det.count()
                    de["Esperando"][carr] = int(de["Volumen"][carr]) - int(de["Confirmados"][carr]) - int(de["No entregados"][carr])

            e.estadisticas = json.dumps(de, ensure_ascii=False)
            e.estado = "terminado"
            e.save()

#	agregar esto solo para probar la funcion como test
#	return HttpResponse("Se han actualizado las estadisticas de los mensajes")
