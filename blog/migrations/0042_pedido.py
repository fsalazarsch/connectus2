# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-11-24 09:59
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0041_auto_20161114_1107'),
    ]

    operations = [
        migrations.CreateModel(
            name='pedido',
            fields=[
                ('id_pedido', models.IntegerField(primary_key=True, serialize=False)),
                ('id_empresa', models.IntegerField()),
                ('cantidad_sms', models.IntegerField()),
                ('cantidad_mail', models.IntegerField()),
                ('token', models.CharField(max_length=255)),
            ],
        ),
    ]
