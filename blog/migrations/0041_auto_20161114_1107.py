# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-11-14 11:07
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0040_auto_20161024_1739'),
    ]

    operations = [
        migrations.CreateModel(
            name='msjinterno',
            fields=[
                ('id_msjinterno', models.IntegerField(primary_key=True, serialize=False)),
                ('id_user', models.IntegerField()),
                ('leido', models.BooleanField()),
                ('tipo', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='reseller',
            fields=[
                ('id_reseller', models.IntegerField(primary_key=True, serialize=False)),
                ('id_empresa_padre', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='valores',
            fields=[
                ('id_valores', models.IntegerField(primary_key=True, serialize=False)),
                ('rango_min', models.IntegerField()),
                ('rango_max', models.IntegerField()),
                ('valor_unitario', models.FloatField()),
                ('tipo', models.IntegerField()),
            ],
        ),
        migrations.RenameField(
            model_name='compra',
            old_name='cantidad_creditos',
            new_name='tipo_compra',
        ),
    ]
