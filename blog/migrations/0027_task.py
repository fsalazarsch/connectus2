# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-08-24 13:08
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0026_auto_20160824_1302'),
    ]

    operations = [
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('task_name', models.CharField(db_index=True, max_length=255)),
                ('task_params', models.TextField()),
                ('task_hash', models.CharField(db_index=True, max_length=40)),
                ('priority', models.IntegerField(db_index=True, default=0)),
                ('run_at', models.DateTimeField(db_index=True)),
                ('attempts', models.IntegerField(db_index=True, default=0)),
                ('failed_at', models.DateTimeField(blank=True, db_index=True, null=True)),
                ('last_error', models.TextField(blank=True)),
                ('locked_by', models.CharField(blank=True, db_index=True, max_length=64, null=True)),
                ('locked_at', models.DateTimeField(blank=True, db_index=True, null=True)),
            ],
            options={
                'db_table': 'background_task',
            },
        ),
    ]
