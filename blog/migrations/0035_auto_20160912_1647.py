# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-09-12 16:47
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0034_auto_20160912_1630'),
    ]

    operations = [
        migrations.AlterField(
            model_name='templatemsg',
            name='id',
            field=models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
        ),
    ]
