# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-09-14 09:51
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0036_merge'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='templatemsg',
            name='id',
        ),
        migrations.AddField(
            model_name='customer',
            name='status',
            field=models.IntegerField(default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='templatemsg',
            name='cuerpo',
            field=models.TextField(default=''),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='profile',
            name='foto',
            field=models.ImageField(default='img/user.png', upload_to='uploads/'),
        ),
        migrations.AlterField(
            model_name='templatemsg',
            name='id_templatemsg',
            field=models.IntegerField(primary_key=True, serialize=False),
        ),
    ]
