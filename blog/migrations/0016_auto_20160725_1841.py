# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-07-25 18:41
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0015_auto_20160725_1839'),
    ]

    operations = [
        migrations.RenameField(
            model_name='codigo_pais',
            old_name='borrado',
            new_name='activo',
        ),
        migrations.RenameField(
            model_name='codigo_pais',
            old_name='id_empresa',
            new_name='codigo',
        ),
        migrations.RenameField(
            model_name='codigo_pais',
            old_name='id_lista',
            new_name='id',
        ),
        migrations.RenameField(
            model_name='codigo_pais',
            old_name='nombre',
            new_name='pais',
        ),
        migrations.RenameField(
            model_name='lista_contacto',
            old_name='activo',
            new_name='borrado',
        ),
        migrations.RenameField(
            model_name='lista_contacto',
            old_name='codigo',
            new_name='id_empresa',
        ),
        migrations.RenameField(
            model_name='lista_contacto',
            old_name='id',
            new_name='id_lista',
        ),
        migrations.RenameField(
            model_name='lista_contacto',
            old_name='pais',
            new_name='nombre',
        ),
        migrations.RemoveField(
            model_name='codigo_pais',
            name='fecha_creacion',
        ),
        migrations.RemoveField(
            model_name='codigo_pais',
            name='id_usuario',
        ),
        migrations.RemoveField(
            model_name='codigo_pais',
            name='ultima_actualizacion',
        ),
        migrations.RemoveField(
            model_name='lista_contacto',
            name='abreviatura',
        ),
        migrations.AddField(
            model_name='codigo_pais',
            name='abreviatura',
            field=models.CharField(default=0, max_length=2),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='lista_contacto',
            name='fecha_creacion',
            field=models.DateTimeField(default=0),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='lista_contacto',
            name='id_usuario',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='lista_contacto',
            name='ultima_actualizacion',
            field=models.DateTimeField(default=0),
            preserve_default=False,
        ),
    ]
