# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-07-07 17:19
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0007_auto_20160629_1453'),
    ]

    operations = [
        migrations.CreateModel(
            name='customer',
            fields=[
                ('customer_id', models.IntegerField(primary_key=True, serialize=False)),
                ('customer_group_id', models.IntegerField()),
                ('firstname', models.CharField(max_length=250)),
                ('lastname', models.CharField(max_length=250)),
                ('email', models.CharField(max_length=100)),
                ('telephone', models.CharField(max_length=100)),
                ('password', models.CharField(max_length=128)),
                ('salt', models.CharField(max_length=128)),
                ('token', models.CharField(max_length=128)),
                ('date_added', models.DateTimeField(auto_now_add=True)),
                ('rut_empresa', models.CharField(max_length=250)),
                ('razon_social', models.CharField(max_length=250)),
            ],
        ),
        migrations.RemoveField(
            model_name='consumo',
            name='id_cuenta_corriente',
        ),
        migrations.AddField(
            model_name='archivos',
            name='tipo',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='compra',
            name='cantidad_mail',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='consumo',
            name='cuenta_corriente',
            field=models.OneToOneField(default=0, on_delete=django.db.models.deletion.CASCADE, related_name='cuenta_corriente', to='blog.cuenta_corriente'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='contacto',
            name='celular',
            field=models.CharField(default=0, max_length=25),
            preserve_default=False,
        ),
    ]
