# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-06-28 20:19
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0004_auto_20160628_1825'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cuenta_corriente',
            name='id_conector_mail',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='conector_mail+', to='blog.conector'),
        ),
        migrations.AlterField(
            model_name='cuenta_corriente',
            name='id_conector_sms',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='conector_sms+', to='blog.conector'),
        ),
    ]
