import os, md5, string, pyexcel, StringIO, json, requests, time, sendgrid, uuid
from django.shortcuts import render
from datetime import datetime, date, timedelta
from django.template import loader, Context
from django.http import HttpResponse, HttpResponseRedirect
from blog.models import archivos, customer, conector, profile, customerAdmin, cuenta_corriente, lista_contacto, contacto, codigo_pais, mensaje, envio, compra, detalle_envio, sms_recibido, estadoenvio, tabla_numeracion, consumo, campo, encolado, msjinterno
import urllib, sys, unicodedata

from django.contrib.auth.models import User, Group

from forms import Formregistro, Formguardarcust, Formimportarlista, Formsms, Formmail, Formcarga, Formuser

from django.shortcuts import render_to_response
from django.template import RequestContext

from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.hashers import make_password

from django.forms.widgets import PasswordInput, TextInput
from blog.views import enviar_sms, escribir_log, escribir_log2

from django.core.paginator import Paginator
from django.core.urlresolvers import reverse, reverse_lazy
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render
from django.db.models import Sum

import xmlrpclib
from SimpleXMLRPCServer import SimpleXMLRPCServer

import cronjobs
from blog.triggers import estadisticas_estado
from blog.views import get_estado, crearcateg, get_cliente_mail

from django.conf import settings
from time import sleep

# descripcion: Pone en cola las listas de email para ser enviadas 
# input: Ninguno
# output: Ninguno
# flujo: toma las listas con failover = -1,  dejando el envio como failover = 1 (listas para ser enviadas)

def pop_envio():
    encolados = encolado.objects.all()
    for enc in encolados:
        e = envio.objects.get(pk = enc.id_envio)

        iemp = profile.objects.get(user_id = enc.id_user).id_empresa.customer_id
        cc = cuenta_corriente.objects.get(pk = iemp)
        conector = cc.id_conector_mail_id
        if int(e.failover) == -1:
            if e.tipo_mensaje == "MAIL":
                campos = campo.objects.values_list('glosa', 'campo').filter(id_lista = enc.id_lista)
                conts = contacto.objects.filter(id_lista = enc.id_lista)
                contador = 0
                if enc.contador != None and int(enc.contador) > 0:
                    contador = int(enc.contador)
                    conts = conts[int(enc.contador):]
                for c in conts:
                    client = get_cliente_mail(conector)
                    message = sendgrid.Mail()
                    categoria = str(enc.id_user)+"-"+str(e.id_envio)+"-"+str((detalle_envio.objects.latest('id_detalle_envio').id_detalle_envio)+1)
                    crearcateg(conector, categoria)
                    message.add_unique_arg("ID_EMPRESA", enc.id_user)
                    message.add_unique_arg("ID_ENVIO", e.id_envio)
                    message.add_unique_arg("DETALLE_ENVIO", (int)(detalle_envio.objects.latest('id_detalle_envio').id_detalle_envio)+1)
                    message.add_category(categoria)
                    try:
                        flag = detalle_envio.objects.get(id_envio = e, id_contacto = int(c.id_contacto)).count()
                    except detalle_envio.DoesNotExist:

                        de = detalle_envio(id_envio = e, id_respuesta_servidor= categoria, estado = "", destinatario = c.email, id_contacto = int(c.id_contacto), fecha = e.cuando_enviar, intentos=0, estado_open=0, estado_click=0, estado_spam =0)
                        de.save()
                        contador = contador +1
                        enc.contador = contador
                        enc.save()
                        escribir_log("MAIL", "enviar", "usuario: "+str(enc.id_user)+"id_envio: "+str(enc.id_envio)+", destinario :"+str(c.email), "ok")

                e.estado ="terminado"

            envio_acumulado = detalle_envio.objects.filter(id_envio = e.id_envio).count()
            nro_cont = contacto.objects.filter(id_lista = enc.id_lista).count()
            if int(envio_acumulado) == int(enc.contador)-1 or int(envio_acumulado) == int(nro_cont):
                e.failover = 1
                e.save()
            
        if e.failover == 1:
            enc.delete()

    restantes = envio.objects.filter(failover = -1)
    for r in restantes:
        try:
            e =  encolado.objects.get(pk = r.id_envio)
        except encolado.DoesNotExist:

            envio_acumulado = detalle_envio.objects.filter(id_envio = r.id_envio).count()
            nro_cont = contacto.objects.filter(id_lista = r.id_lista).count()
            if int(envio_acumulado) == int(nro_cont):
                r.failover = 1
                r.save()


# descripcion: TEST obtine el estado de servidor teleco
# input: <HttpRequest>request, <int>idresp
# output: <HttpResponse> arreglo de estado de servidor, segun la respuesta de servidor (idresp)
# flujo: hace una llamada al servidor y muestra la respuesta

def testear_retorno_sms(request, idresp):
    server_url = 'http://smpp2.telecochile.cl:4040'
    server = xmlrpclib.Server(server_url);
    res2 = server.enquireMsgStatus('MConnectus','matias2015', idresp)
    escribir_log("Estado sms", str(res2).strip('[]'), "id_envio", "ok")
    return HttpResponse(str(res2).strip('[]'))


# descripcion: obtiene los numeros y su mensaje de quienes responden al envio
# input: Ninguno
# output: Ninguno
# flujo: guarda los resutados en la tabla  sms recibido

def testear_sms_recibidos(): 
    server_url = 'http://smpp2.telecochile.cl:4040'
    server = xmlrpclib.Server(server_url);
    res2 = server.getReceivedMessages('MConnectus','matias2015')

    for r in res2['MESSAGES']:
        try:
            sms_recibido.objects.get(id_respuesta_servidor = r['id'])
        except sms_recibido.DoesNotExist:
            s = sms_recibido(id_respuesta_servidor = r['id'], fecha = r['arrived'], mensaje = r['message'], destinatario= r['to'], remitente= r['from'], borrado = 0)
            escribir_log("sms recibido", str(r), str(r['id']), "ok")
            s.save()

# descripcion: determina los estados por teleco o sengrid, segun el conector
# input: <int>detalle_env, <String>tipo, <int>conector
# output: Ninguno
# flujo: guarda el resultado en detalle_envio.estado

def procesar_detalle_envio(detalle_env, tipo, conector =0):
    det = detalle_envio.objects.get(pk= detalle_env)
    if conector == 1:
        if tipo == 'sms' and det.id_respuesta_servidor != "":
            server_url = 'http://smpp2.telecochile.cl:4040'
            server = xmlrpclib.Server(server_url);
            res2 = server.enquireMsgStatus('MConnectus','matias2015', det.id_respuesta_servidor)
            det.estado = res2["STATUS"]
        
            if res2['CODE']  == 4 or res2['CODE'] == 5:
                det.estado = "INVALID_DNS"
            elif res2['CODE'] == -1:
                det.estado = "ERROR"
            else:
                det.estado = res2["STATUS"]
            escribir_log("Estado sms", str(det.id_respuesta_servidor)+"--"+str(det.estado), "envio valorizado", "ok")
        
        det.intentos = 1
        det.save()
    if tipo == 'mail' and det.estado == 'UNKNOWN':
        #determinar estado por sendgrid    
        det.estado = get_estado(conector, det.id_respuesta_servidor)
        det.intentos = 1
        det.save()

# descripcion: procesa los envios
# input: <int>id_envio, <int>tipo_envio
# output: Ninguno
# flujo: prcesa los envios para parsearlos en la funcion anterior, y pone el failover = 0

def procesar_envio(id_envio, tipo_envio): #tipo_envio mail o sms

    iemp = envio.objects.get(pk= id_envio).id_empresa
    cc = cuenta_corriente.objects.get(pk = iemp)
    conector = cc.id_conector_mail_id

    det = detalle_envio.objects.filter(id_envio = id_envio)
    for d in det:
        procesar_detalle_envio(d.id_detalle_envio, tipo_envio, conector)
    estadisticas_estado()
    env = envio.objects.get(pk = id_envio)
    env.estado = "terminado"
    env.failover = 0

# descripcion: crea las estadisticas de los envios
# input: Ninguno
# output: Ninguno
# flujo: determina el tipo, y manda a procesar el envio

def realizar_estadisticas():
    pass
    tipo =""
    
    env = envio.objects.all()
    for e in env:
        if e.estadisticas == None or e.estadisticas == "" or e.estado != 'terminado' or e.estado == '':
            if e.correo_remitente == "" and e.remitente != '':
                tipo = "sms"
            else:
                tipo="mail"
            procesar_envio(e.id_envio, tipo)

def guardar_nuevo_estado_mail():
    pass
    time_threshold = datetime.now() - timedelta(hours=48)
    envios = envio.objects.filter(cuando_enviar__gt=time_threshold)
    for e in envios:
        if e.correo_remitente == "" and e.remitente != '':
            tipo = "sms"
        else:
            tipo="mail"

        iemp = envio.objects.get(pk = e.id_envio).id_empresa
        cc = cuenta_corriente.objects.get(pk = iemp)
        conector = cc.id_conector_mail_id

        detalles = detalle_envio.objects.filter(id_envio = e.id_envio)
        for det in detalles:
            if det.id_respuesta_servidor  != "" and tipo == 'sms':
                if conector == 1:
                    if det.estado == "WAITING FOR CONFIRMATION" or det.estado == "UNKNOWN" or (det.estado =="" and det.id_respuesta_servidor != -1):
                    #reemplazamos el estado y lo guardamos
                        server_url = 'http://smpp2.telecochile.cl:4040'
                        server = xmlrpclib.Server(server_url);
                        res2 = server.enquireMsgStatus('MConnectus','matias2015', det.id_respuesta_servidor)
                        det.estado = str(res2['STATUS'])
                        det.save()
                        #reseteamos la estadistica
                        e.estado = "actualizando"
                        e.save()
            if det.id_respuesta_servidor != "" and tipo == 'mail':
                if det.estado == "UNKNOWN" or det.estado =="":
                    det.estado =  get_estado(conector, det.id_respuesta_servidor)
                    det.save()
                    #reseteamos la estadistica
                    e.estado = "actualizando"
                    e.save()
    estadisticas_estado()


# descripcion: envia los sms con envio.failover = 1
# input: Ninguno
# output: Ninguno
# flujo: ejecuta el envio

def envio_sms():
    pass
    envid = envio.objects.latest('id_envio')
    pendientes = envio.objects.filter(failover= 1)
    pendientes = pendientes.filter(tipo_mensaje = "SMS")
    pendientes = pendientes.filter(cuando_enviar__lt = datetime.now())
    pendientes = pendientes.order_by('-cuando_enviar')

    if pendientes:
        for p in pendientes:
            if p.tipo_envio == "unico":
                j = json.loads(p.datos_envio_programado)
                try:
                    detalle_env = detalle_envio.objects.get(id_envio = p.id_envio)

                    cc = cuenta_corriente.objects.get(pk = p.id_empresa)
                    conector = cc.id_conector_sms_id
                    if int(detalle_env.intentos) != 1:
                        enviar_sms(conector, str(j['destinatario']), j['mensaje_a_enviar'], p.id_envio, "unico")
                except:
                    escribir_log("Enviar sms", "Error en accion enviar_sms", "id_envio: "+str(p.id_envio), "ok")

            if p.tipo_envio == "masivo":                
                campos = campo.objects.filter(id_lista = p.id_lista)
                j = json.loads(p.datos_envio_programado)
                
                destinatarios = contacto.objects.filter(id_lista = p.id_lista)
                cc = cuenta_corriente.objects.get(pk = p.id_empresa)
                conector = cc.id_conector_sms_id

                mens = []
                cels = []

                for c in destinatarios:
                    mje = j["mensaje_a_enviar"]

                    for ca in campos:
                        if ca.campo.lower() == "nombre":
                            mje = mje.replace("%nombre%", c.nombre)
                        elif ca.campo.lower() == "email":
                            mje = mje.replace("%email%", c.email)
                        elif ca.campo.lower() == "mail":
                            mje = mje.replace("%mail%", c.email)
                        elif ca.campo.lower() == "mails":
                            mje = mje.replace("%mails%", c.email)
                        elif ca.campo.lower() == "emails":
                            mje = mje.replace("%emails%", c.email)

                        elif ca.campo.lower() == "celular":
                            mje = mje.replace("%celular%", c.celular)
                        elif ca.campo.lower() == "fecha_nac":
                            mje = mje.replace("%fecha_nac%", c.fecha_nac)
                        elif ca.campo.lower() == "sexo":
                            mje = mje.replace("%sexo%", c.sexo)
                        else:
                            custom = contacto.objects.values_list('custom', flat=True).get(pk = c.id_contacto)
                            custom = json.loads(custom)

                            mje = mje.replace("%"+ca.glosa+"%", custom[str(ca.campo)])
                    cels.append(c.celular)
                    mens.append(mje)
                enviar_sms(conector, cels, mens, p.id_envio, "masivo")

            env = envio.objects.get(id_envio = p.id_envio)
            env.failover = 0
            env.estado = "Terminado"
            env.save()
            
# descripcion: envia los mail con envio.failover = 1
# input: Ninguno
# output: Ninguno
# flujo: ejecuta el envio

def enviar_mail():
    pass
    rebotes = 0
    pendientes = envio.objects.filter(failover= 1)
    pendientes = pendientes.filter(tipo_mensaje = "MAIL")
    pendientes = pendientes.filter(cuando_enviar__lt = datetime.now())
    penduentes = pendientes.order_by('-cuando_enviar')

    if pendientes:
        for p in pendientes:
            iemp = p.id_empresa

            cc = cuenta_corriente.objects.get(pk = iemp)
            conector = cc.id_conector_mail_id

            client = get_cliente_mail(conector)
            message = sendgrid.Mail()


            if p.tipo_envio == "unico":
                j = json.loads(p.datos_envio_programado)
                conts= []
                detalles_envios = detalle_envio.objects.filter(id_envio = p.id_envio)

                for det in detalles_envios:
                    args = det.id_respuesta_servidor.split("-")
                    crearcateg(conector, det.id_respuesta_servidor)

                    message.add_to(j['destinatario'])
                    message.set_from(j['nombre_remitente']+'<'+j['remitente']+'>')
                    message.set_subject(j['asunto'])
                    message.add_unique_arg("ID_EMPRESA", args[0])
                    message.add_unique_arg("ID_ENVIO", args[1])
                    message.add_unique_arg("DETALLE_ENVIO", args[2])
                    message.add_category(det.id_respuesta_servidor)

                    header = '<p>Para asegurar la entrega de nuestros e-mail en su correo, por favor agregue '+str(j['remitente'])+' a su libreta de direcciones<br>Si usted no visualiza bien este mail, haga click <a target="_blank" href="http://35.160.72.247/viewmail/0/'+str(p.id_envio)+'/">aqui</a></p>'
                    footer = '<p>Este correo electronico fue enviado a '+str(j['destinatario'])
                    message.set_html(header+str(j['mensaje_a_enviar'].encode('utf-8'))+footer)

                    if int(det.intentos) != 1 and det.estado != 0:
                        client.send(message)
                        escribir_log("mail enviado - programado", str(det.id_respuesta_servidor) ,"" , "ok")

                        det.intentos = 1
                        det.save()
                        det.estado = ''
                        det.save()
            else:
                conts = contacto.objects.filter(id_lista = p.id_lista)
                campos =  campo.objects.values_list('glosa', 'campo').filter(id_lista = p.id_lista)
                j = json.loads(p.datos_envio_programado)
                de = detalle_envio.objects.filter(id_envio = p.id_envio)
                aux = []
                
                flag = False
                idenv = p
                i =0
                if not idenv.envios_malos:
                    i = 0
                    idenv.envios_malos = 0
                    idenv.save()
                else:
                    i = idenv.envios_malos
                    flag = True
                
                for d in de:
                    if d.intentos != 1 and de.count() >= i and not d.id_detalle_envio in aux and flag == False:

                        det = d.id_respuesta_servidor
                        client = get_cliente_mail(conector)
                        message = sendgrid.Mail()

                        args = det.split("-")
                        crearcateg(conector, det)

                        c = contacto.objects.get(pk = d.id_contacto)


                        message.add_to(c.email)
                        message.set_from(j['nombre_remitente']+'<'+j['remitente']+'>')
                        message.set_subject(j['asunto'])


                        estado =""
                        mje = j['mensaje_a_enviar']
                        for ca in campos:
                            if ca[0].lower() == "nombre":
                                mje = mje.replace("%nombre%", c.nombre)
                            elif ca[0].lower() == "email":
                                mje = mje.replace("%email%", c.email)
                            elif ca[0].lower() == "mail":
                                mje = mje.replace("%mail%", c.email)
                            elif ca[0].lower() == "mails":
                                mje = mje.replace("%mails%", c.email)
                            elif ca[0].lower() == "emails":
                                mje = mje.replace("%emails%", c.email)

                            elif ca[0].lower() == "celular":
                                mje = mje.replace("%celular%", c.celular)
                            elif ca[0].lower() == "fecha_nac":
                                mje = mje.replace("%fecha_nac%", c.fecha_nac)
                            elif ca[0].lower() == "sexo":
                                mje = mje.replace("%sexo%", c.sexo)
                            else:
                                custom = json.loads(c.custom)
                                mje = mje.replace("%"+ca[0]+"%", unicode(custom[str(ca[1])]))

                        mje = mje.replace("../", "")
                        mje = mje.replace("static", "http://35.160.72.247/static")

                        header = '<p style="font-size:smaller; text-align: center;">Para asegurar la entrega de nuestros e-mail en su correo, por favor agregue '+str(j['remitente'])+' a su libreta de direcciones<br>Si usted no visualiza bien este mail, haga click <a target="_blank"  href="http://35.160.72.247/viewmail/'+str(c.id_contacto)+'/'+str(p.id_envio)+'/">aqui</a></p>'
                        footer = '<p style="font-size:smaller; text-align: center;">Este correo electronico fue enviado a '+str( unicodedata.normalize('NFKD', c.email).encode('ascii', 'ignore').encode('utf-8'))+'.<br>Para anular tu suscripcion haz click<a href="http://35.160.72.247/desinscribir/'+str(c.id_contacto)+'-'+str(uuid.uuid5(uuid.NAMESPACE_DNS, str(c.id_contacto)))+'/">aqui</a></p>'

                        message.set_html(header+mje+footer)

                        message.add_unique_arg("ID_EMPRESA", args[0])
                        message.add_unique_arg("ID_ENVIO", args[1])
                        message.add_unique_arg("DETALLE_ENVIO", args[2])
                        message.add_category(det)

                        resp = client.send(message)
                        escribir_log2("progresoenvio"+str(p.id_envio), "mail enviado - programado", "", " ", str(i))

                        d.intentos = 1
                        d.save()
                        i= i+1
                        idenv.envios_malos = i
                        idenv.save()
                    else:
                        i=i+1

                p.failover = 1
                p.save()

            if p.tipo_envio == "unico" or int(p.envios_malos) > int(conts.count()):
                p.failover = 0
            else:
                p.failover = 1

            if p.tipo_envio != "unico" and int(p.envios_malos) == int(conts.count()):
                p.failover = 1

            p.save()

# descripcion: crea el reporte mensual
# input: <HttpRequest>request
# output: Ninguno
# flujo: crea en tiepo real un excel con envios

def reporte_mes(request):
    pass

    if request.user.is_staff:
        hoy = datetime.now() 
        envios = envio.objects.all()
        if int(hoy.month) == 1:
            anio = int(hoy.year) -1
            mes = 12
        else:
            anio = int(hoy.year)
            mes = int(hoy.month) -1
        
        envios = envios.filter(cuando_enviar__year = anio, cuando_enviar__month = mes )
        data =[]

        header = ["Id", "Empresa", "ID Detalle Envio", "ID Envio", "Estado", "tipo envio", "tipo mensaje", "Estado envio", "cuano enviar", "conector", "Nombre", "grupo", "idmensaje"]
        data.append(header)

                
        for e in envios:      
            fila = ["","",0,0,"","","","","","","","",0]
            fila[0] = e.id_empresa
            fila[3] = e.id_envio
            fila[4] = e.estado
            fila[5] = e.tipo_envio
            fila[6] = e.tipo_mensaje
            fila[8] = e.cuando_enviar
            fila[12] = e.id_mensaje
            try:
                fila[9] = conector.objects.get(pk = e.id_conector).glosa
            except conector.DoesNotExist:
                fila[9] = "---"

            try:
                fila[1] = customer.objects.get(pk = e.id_empresa).razon_social
                fila[10] = customer.objects.get(pk = e.id_empresa).firstname
            except customer.DoesNotExist:
                fila[1] = "---"
                fila[10] = "---"
    
            try:
                u = User.objects.get(pk = e.id_usuario)
                if u.is_superuser:
                    fila[11] = "Reseller"
                else:
                    fila[11] = "Cliente"
            except User.DoesNotExist:
                fila[11] = "---"

            try:
                for d in detalle_envio.objects.filter(id_envio = e.id_envio):
                    fila[2] = d.id_detalle_envio
                    fila[7] = d.estado
                    data.append(fila)

            except detalle_envio.DoesNotExist:
                fila[2] = 0
                fila[7] = "---"
                data.append(fila)



        sheet = pyexcel.Sheet(data)
        io = StringIO.StringIO()
        d = datetime.now().date
        pyexcel.save_as(array=data, dest_file_name=settings.STATIC_ROOT+settings.STATIC_URL+"files/"+str(anio)+"-"+str(mes)+".xlsx")
        
        if archivos.objects.all().filter(ruta = str(anio)+"-"+str(mes)+".xlsx").count() == 0:
            arch = archivos(nombre= "Reporte Facturacion Completo " + str(anio)+"-"+str(mes), ruta= str(anio)+"-"+str(mes)+".xlsx", creacion= datetime.now(), tipo_archivo= 'rep', tipo= request.user.id)
            arch.save()

# descripcion: elimina los archivos que pasen los 7 dias de antiguedad
# input: Ninguno
# output: Ninguno
# flujo: ---

def eliminar_archs():
    pass
    #elimina los archivos que pasen los 7 dias de antiguedad
    ruta = settings.STATIC_ROOT+settings.STATIC_URL+"files/"
    d = datetime.now() - timedelta(days=7)
    archs = archivos.objects.filter(creacion__lt= d)
    for a in archs:
        if os.path.isfile(ruta+a.ruta):
            os.remove(ruta+a.ruta)
        a.delete()


# descripcion: refresca los estados de no mas de un dia 
# input: Ninguno
# output: Ninguno
# flujo: obtiene los envios de hasta ayer, le actualiza los estados y recalcula la estadistica

def refrescar_estado():
    pass
    ayer = datetime.now() - timedelta(days=1)
    ayer.strftime('%y-%m-%d')
    envios = envio.objects.filter(cuando_enviar__gte = ayer)
    envios = envios.filter(failover = 0)

    aux =""
    estadosok = estadoenvio.objects.values('estado_raw').filter(id_estadohum = 3)
    estadoserror = estadoenvio.objects.values('estado_raw').filter(id_estadohum = 1) | estadoenvio.objects.values('estado_raw').filter(id_estadohum=4)
    rebote = estadoenvio.objects.values('estado_raw').filter(id_estadohum = 2)
    estadosespera = estadoenvio.objects.values('estado_raw').filter(id_estadohum = 0)

    for e in envios:
        try:
            env = envio.objects.get(pk=e.id_envio)

            aux += str(e.id_envio)+" "

            if env.tipo_mensaje == "SMS":
                est = json.loads(env.estadisticas)
                detalles = detalle_envio.objects.filter(id_envio = e.id_envio)
                for det in detalles:
                    idresp = det.id_respuesta_servidor
                    server_url = 'http://smpp2.telecochile.cl:4040'
                    server = xmlrpclib.Server(server_url);
                    res2 = server.enquireMsgStatus('MConnectus','matias2015', idresp)
                    det.estado = str(res2['STATUS'])
                    det.save()


                #resetear estadisticas
                carriers = detalles.values_list('empresa_telefono_receptor', flat=True).distinct()

                #crear json mail     
                de = {}
                de["Volumen"] = {}
                de["Esperando"] = {}
                de["Confirmados"] = {}
                de["No entregados"] = {}

                de["Confirmados"]['Total'] = detalles.filter(estado__in= estadosok).count()
                de["Esperando"]['Total'] = int(de["Volumen"]['Total']) - int(de["Confirmados"]['Total']) - int(de["No entregados"]['Total'])
                de["Volumen"]['Total'] = detalles.count()
                de["No Entregados"]['Total'] =  detalles.filter(estado__in= estadoserror).count() + detalles.filter(estado__in= rebote).count()
                for carr in carriers:
                    det = detalle_envio.objects.filter(id_envio = id, empresa_telefono_receptor = carr)
                    de["Confirmados"][carr] = det.filter(estado__in= estadosok).count()
                    de["Esperando"][carr] = int(de["Volumen"][carr]) - int(de["Confirmados"][carr]) - int(de["No entregados"][carr])
                    de["Volumen"][carr] = det.count()
                    de["No Entregados"][carr] = det.filter(estado__in= estadoserror).count() + det.filter(estado__in= rebote).count()

                env.estadisticas = json.dumps(de, ensure_ascii=False)
                env.estado = "terminado"
                env.save()

            #ahora mail
            contador = 0
            if env.tipo_mensaje == "MAIL":

                iemp = env.id_empresa
                cc = cuenta_corriente.objects.get(pk = iemp)
                conector = cc.id_conector_mail_id

                detalles = detalle_envio.objects.filter(id_envio = id)
                for det in detalles:
                    idresp = det.id_respuesta_servidor
                    det.estado =  get_estado(conector, idresp)
                    det.save()
                    if det.estado =="":
                        contador += 1


                de = {}
                de["Volumen"] = detalles.count()
                de["Entregados"] = detalles.filter(estado__in= estadosok).count()
                de["Malos"] = detalles.filter(estado__in= estadoserror).count() + detalles.filter(estado__in= estadosespera).count()
                de["Rebotes"] = detalles.filter(estado__in= rebote).count()
                de["Esperando"] = de["Volumen"] - ( de["Entregados"] + de["Malos"] + de["Rebotes"])
                env.estadisticas = json.dumps(de, ensure_ascii=False)
                env.save()
            escribir_log("Refrescar Estado", str(aux), "id_envio", "ok")
        except:
            flag = None


# descripcion: establece los graficos en todas las empresas
# input: Ninguno
# output: Ninguno
# flujo: realiza un foreach en cada empresa (ver grafico_envios)

def establecer_grafico():
    pass
    empresas = customer.objects.all()
    for e in empresas:
        grafico_envios(e.customer_id)
    
# descripcion: establece los graficos en una empresa
# input: <int>id
# output: Ninguno
# flujo: si esta vacio crea un json, si es la empresa es 132 (Connectus), hace un grafico de todos los envios  

def grafico_envios(id):
    if str(id) == 132:
        cont =  detalle_envio.objects.all().count()
    else:
        cont = detalle_envio.objects.filter(id_envio__id_empresa = id).count()
    cust = customer.objects.get(pk = id)
    try:
        piv = json.loads(cust.envios)
    except:
        piv = None
    
    if  piv["cont"] != cont or piv == None:
        

        now = datetime.now()
        envios = {}
    ##crear por anio
        envios["anio"] = {}
        envios["anio"]["sms"] = {}
        envios["anio"]["mail"] = {}

        if int(id) == 132:
            envios["cont"] = detalle_envio.objects.all().count()
        else:
            envios["cont"] = detalle_envio.objects.filter(id_envio__id_empresa = int(id)).count()

        for i in range(1, 13):

            try:
                fe = datetime.strptime(str(now.year)+'-'+str(i)+'-'+str(now.day), "%Y-%m-%d")
                if int(id) == 132:
                    aux = detalle_envio.objects.filter(id_envio__cuando_enviar__year = now.year, id_envio__cuando_enviar__month = i)
                else:
                    aux = detalle_envio.objects.filter(id_envio__cuando_enviar__year = now.year, id_envio__cuando_enviar__month = i, id_envio__id_empresa = int(id))
                saux = aux.filter(id_envio__tipo_mensaje="sms").count()
                maux = aux.filter(id_envio__tipo_mensaje="mail").count()

                envios["anio"]["sms"][str(i)] =saux
                envios["anio"]["mail"][str(i)] =maux

            except:
                envios["anio"]["sms"][str(i)] = 0
                envios["anio"]["mail"][str(i)] = 0


     ##crear por mes
        envios["mes"] = {}
        envios["mes"]["sms"] = {}
        envios["mes"]["mail"] = {}

        for i in range(1, 32):
            try:
                fe = datetime.strptime(str(now.year)+'-'+str(now.month)+'-'+str(i), "%Y-%m-%d")
                fe = str(fe)[0:10]

                if int(id) == 132:
                    aux = detalle_envio.objects.all()
                    aux = detalle_envio.objects.filter(id_envio__cuando_enviar__year = now.year, id_envio__cuando_enviar__month = now.month, id_envio__cuando_enviar__day = i)
                else:
                    aux = detalle_envio.objects.filter(id_envio__cuando_enviar__year = now.year, id_envio__cuando_enviar__month = now.month,  id_envio__cuando_enviar__day = int(i) , id_envio__id_empresa= int(id))
                saux = aux.filter(id_envio__tipo_mensaje="sms").count()
                maux = aux.filter(id_envio__tipo_mensaje = "mail").count()
            except:
                maux = 0
                saux = 0
            envios["mes"]["sms"][str(i)] = saux
            envios["mes"]["mail"][str(i)] = maux

     ## crear por dia
        envios["dia"] = {}
        envios["dia"]["sms"] = {}
        envios["dia"]["mail"] = {}

        for i in range(0, 24):
            if int(id) == 132:
                aux = detalle_envio.objects.filter(id_envio__cuando_enviar__date = datetime.now(), id_envio__cuando_enviar__hour= i) 
            else:
                aux = detalle_envio.objects.filter(id_envio__cuando_enviar__date = datetime.now(), id_envio__cuando_enviar__hour= i, id_envio__id_empresa= id)
           
            saux = aux.filter(id_envio__tipo_mensaje = "SMS").count()
            maux = aux.filter(id_envio__tipo_mensaje = "MAIL").count()

            envios["dia"]["sms"][str(i)] = saux
            envios["dia"]["mail"][str(i)] = maux

        cust = customer.objects.get(pk = id)
        cust.envios = json.dumps(envios)
        cust.save()


# descripcion: elimina las notificaciones (msjinterno)
# input: Ninguno
# output: Ninguno
# flujo: ---  

def eliminar_notificaciones():
    notif = msjinterno.objects.all()
    for n in notif:
        n.delete()


# descripcion: usa la funcion crear_reporte y redirige 
# input: <HttpRequest>request, <int>entrar_como
# output: <HttpResponse> url
# flujo: ---

def solicitar_reporte(request, entrar_como=0):
    reporte_mes(request)
    if entrar_como == 0 or entrar_como == None:
        return HttpResponseRedirect("/perfil/reporte/rep/")
    else:
        return HttpResponseRedirect("/perfil/reporte/rep/"+str(entrar_como))

