"""connectus2 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import patterns, url, include
from django.conf.urls import handler400, handler403, handler404, handler500

from django.contrib import admin
from django.contrib.auth.views import login, logout

from blog.views import index
from blog.views import smsmasivo
from blog.views import mailmasivo
from blog.views import plataforma
from blog.views import contactof
from blog.views import logs, desc_log, webmail, desinscribir, failover
from blog.views import registro, perfil, cargar, cargar2, get_upload, upload2,  confirmar_compra, eliminar_compra, movimientos


from blog.views import error_404
from django.conf.urls.static import static

from django.conf.urls import url
from django import views

from blog.views import archivo, delete_archivo, confirmar_registro, save_conector, pedidos, get_estado, recuperar, confirmar_restablecer, ref_estad, ref_estad_masivo
from blog.views import cliente, edit_cliente, save_cliente, deshab_cliente
from blog.views import estadenv, edit_estadenv, agregar_estadenv, delete_estadenv
from blog.views import user, edit_user, create_user, editp_user, delete_upload, delete_user
from blog.views import lista, lista_excel, ver_lista, delete_lista, create_lista, importar_lista, guardar_lista, exportar_lista
from blog.views import mail,  sms_enviar, predefinido, delete_msj, edit_msj, programado, historico, pred_excel, agregar_msj, desinscrito, delete_envio, progr_excel, recibidos, detalle_historico, reporte_historico, excel_historico
from blog.views import get_contacto, editar_contacto, crear_contacto, subir_archivos, crear_archivo, eliminar_contacto
from blog.views import templatesmsj, edit_templatemsg, agregar_templatemsg, delete_templatemsg, lista_negra, edit_blacklist, agregar_blacklist, delete_blacklist
from blog.views import listar_bug, responder_bug, msjinterno_delete, ver_bug, agregar_bug, delete_bug, ver_bug2, agregar_bug2, responder_bug2
from blog.views import conversor

from blog.api import send_email, get_credits, send_sms, send_sms_snd, get_status_msj_sent

from blog.triggers import estadisticas_estado

from blog.cron import envio_sms, enviar_mail, reporte_mes, testear_retorno_sms, solicitar_reporte, establecer_grafico

handler404 = 'blog.views.error_404'
#handler500 = 'blog.views.error_500'
#handler403 = 'blog.views.error_403'
#handler400 = 'blog.views.error_400'

urlpatterns = [
    url(r'^admin/', admin.site.urls),

    url(r'^$', index),
    url(r'^index/', index),
    url(r'^sms-masivo/', smsmasivo),
    url(r'^mail-masivo/', mailmasivo),
    url(r'^plataforma/', plataforma),
    url(r'^contacto/', contactof),

    url(r'^registro/', registro),
    url(r'^recuperar/', recuperar),
    
    url(r'^perfil/$', perfil),
    url(r'^perfil/(?P<entrar_como>\d+)/$', perfil),
    url(r'^perfil/upload/$', upload2),

    url(r'^perfil/failover/$', failover),
    url(r'^perfil/failover/(?P<entrar_como>\d+)/$', failover),

    url(r'^perfil/logs/$', logs),
    url(r'^perfil/logs/(?P<entrar_como>\d+)/$', logs),
    url(r'^perfil/logs/descargar/(?P<nom>\w+)/$', desc_log),
    url(r'^perfil/logs/(?P<entrar_como>\d+)/descargar/(?P<nom>\w+)/$', desc_log),


    url(r'^perfil/subir_archivos/$', subir_archivos),
    url(r'^perfil/subir_archivos/_(?P<resp>\d+)/$', subir_archivos),
    url(r'^perfil/subir_archivos/(?P<entrar_como>\d+)/$', subir_archivos),
    url(r'^perfil/subir_archivos/(?P<entrar_como>\d+)/_(?P<resp>\d+)/$', subir_archivos),

    url(r'^perfil/archivo/delete/(?P<id>\d+)/$', delete_upload),
    url(r'^perfil/archivo/delete/(?P<id>\d+)/(?P<entrar_como>\d+)$', delete_upload),

    url(r'^perfil/uploads/create/$', crear_archivo),
    url(r'^perfil/uploads/create/(?P<entrar_como>\d+)/$', crear_archivo),

    url(r'^perfil/msjinterno_delete/(?P<id>\d+)/$', msjinterno_delete),

    url(r'^perfil/cargar/$', cargar),
    url(r'^perfil/cargar/_(?P<resp>\d+)/$', cargar),
    url(r'^perfil/cargar/(?P<entrar_como>\w+)/_(?P<resp>\d+)/$', cargar),
    url(r'^perfil/cargar/(?P<entrar_como>\w+)/$', cargar),
    url(r'^perfil/movimientos/$', movimientos),
    url(r'^perfil/pedidos/$', pedidos),
    url(r'^perfil/pedidos/_(?P<responses>\d+)/$', pedidos),
    url(r'^perfil/conversor/$', conversor),

    url(r'^perfil/comprar/$', cargar2),
    url(r'^perfil/comprar/(?P<responses>\d+)/$', cargar2),
    url(r'^perfil/confirmar_compra/$', confirmar_compra),
    url(r'^perfil/eliminar_compra/(?P<id>\d+)/$', eliminar_compra),


    url(r'^perfil/reporte/delete/(?P<id>\d+)/$', delete_archivo),
    url(r'^perfil/reporte/delete/(?P<id>\d+)/(?P<entrar_como>\d+)/$', delete_archivo),

    url(r'^perfil/reporte/$', archivo),
    url(r'^perfil/reporte/solicitar/$', solicitar_reporte),
    url(r'^perfil/reporte/solicitar/(?P<entrar_como>\d+)/$', solicitar_reporte),
    url(r'^perfil/reporte/(?P<response>\w+)/$', archivo),
    url(r'^perfil/reporte/(?P<response>\w+)/(?P<entrar_como>\d+)/$', archivo),
    url(r'^perfil/reporte/(?P<response>\w+)/(?P<entrar_como>\d+)/_(?P<responses>\d+)/$', archivo),
    url(r'^perfil/reporte/(?P<response>\w+)/_(?P<responses>\d+)/$', archivo),


    url(r'^perfil/user/$', user),
    url(r'^perfil/user/(?P<responses>\d+)/$', user),
    url(r'^perfil/user/(?P<responses>\d+)/_(?P<entrar_como>\d+)/$', user),
    url(r'^perfil/user/_(?P<entrar_como>\d+)/$', user),
    url(r'^perfil/user/create/$', create_user),
    url(r'^perfil/user/(?P<responses>\d+)/create/$', create_user),
    url(r'^perfil/user/(?P<responses>\d+)/_(?P<entrar_como>\d+)/create/$', create_user),
    url(r'^perfil/user/_(?P<entrar_como>\d+)/create/$', create_user),
    url(r'^perfil/user/edit/(?P<id>\d+)/$', edit_user),
    url(r'^perfil/user/edit/(?P<id>\d+)/_(?P<entrar_como>\d+)/$', edit_user),
    url(r'^perfil/user/editp/(?P<id>\d+)/$', editp_user),
    url(r'^perfil/user/editp/(?P<id>\d+)/_(?P<entrar_como>\d+)/$', editp_user),
    url(r'^perfil/user/delete/(?P<id>\d+)/_(?P<entrar_como>\d+)/$', delete_user),
    url(r'^perfil/user/delete/(?P<id>\d+)/$', delete_user),



    url(r'^perfil/cliente/$', cliente),
    url(r'^perfil/cliente/edit/(?P<id>\d+)/$', edit_cliente),
    url(r'^perfil/cliente/edit/(?P<id>\d+)/_(?P<entrar_como>\d+)/$', edit_cliente),
    url(r'^perfil/cliente/save/(?P<id>\d+)/$', save_cliente),
    url(r'^perfil/cliente/create/$', save_cliente),
    url(r'^perfil/cliente/create/(?P<entrar_como>\d+)/$', save_cliente),
    url(r'^perfil/cliente/delete/(?P<id>\d+)/$', deshab_cliente),
    url(r'^perfil/cliente/(?P<responses>\d+)/$', cliente),
    url(r'^perfil/cliente/_(?P<entrar_como>\d+)/$', cliente),
    url(r'^perfil/cliente/(?P<responses>\d+)/_(?P<entrar_como>\d+)/$', cliente),
    url(r'^perfil/conector/guardar/$', save_conector),
    #url(r'^perfil/conector/$', cliente),
    

    url(r'^perfil/lista/$', lista),
    url(r'^perfil/lista/excel/$', lista_excel),
    url(r'^perfil/lista/edit/(?P<id>\d+)/$', ver_lista),
    url(r'^perfil/lista/edit/(?P<id>\d+)/_(?P<resp>\d+)/$', ver_lista),

    url(r'^perfil/lista/delete/(?P<id>\d+)/$', delete_lista),
    url(r'^perfil/lista/create/$', create_lista),
    url(r'^perfil/lista/importar/$', importar_lista),
    url(r'^perfil/lista/guardar/$', guardar_lista),

    url(r'^perfil/lista/(?P<entrar_como>\d+)/$', lista),
    url(r'^perfil/lista/_(?P<resp>\d+)/$', lista),
    url(r'^perfil/lista/(?P<entrar_como>\d+)/_(?P<resp>\d+)/$', lista),

    url(r'^perfil/lista/edit/(?P<id>\d+)/(?P<entrar_como>\d+)/$', ver_lista),
    url(r'^perfil/lista/edit/(?P<id>\d+)/(?P<entrar_como>\d+)/_(?P<resp>\d+)/$', ver_lista),

    url(r'^perfil/lista/edit/(?P<id>\d+)/$', ver_lista),
    url(r'^perfil/lista/exportar/(?P<id>\d+)/(?P<entrar_como>\d+)/$', exportar_lista),
    url(r'^perfil/lista/exportar/(?P<id>\d+)/$', exportar_lista),

    url(r'^perfil/lista/delete/(?P<id>\d+)/(?P<entrar_como>\d+)/$', delete_lista),
    url(r'^perfil/lista/create/(?P<entrar_como>\d+)/$', create_lista),
    url(r'^perfil/lista/excel/(?P<entrar_como>\d+)/$', lista_excel),
    url(r'^perfil/lista/importar/(?P<entrar_como>\d+)/$', importar_lista),
    url(r'^perfil/lista/guardar/(?P<entrar_como>\d+)/$', guardar_lista),
            
    url(r'^perfil/contacto/edit/(?P<id>\d+)/$', editar_contacto),
    url(r'^perfil/contacto/edit/(?P<id>\d+)/(?P<entrar_como>\d+)/$', editar_contacto),

    url(r'^perfil/contacto/crear/(?P<id>\d+)/$', crear_contacto),
    url(r'^perfil/contacto/crear/(?P<id>\d+)/(?P<entrar_como>\d+)/$', crear_contacto),

    url(r'^perfil/contacto/delete/(?P<id>\d+)/$', eliminar_contacto),
    url(r'^perfil/contacto/delete/(?P<id>\d+)/(?P<entrar_como>\d+)/$', eliminar_contacto),


    url(r'^perfil/templatemsg/$', templatesmsj),
    url(r'^perfil/templatemsg/(?P<entrar_como>\d+)/$', templatesmsj),
    url(r'^perfil/templatemsg/(?P<entrar_como>\d+)/_(?P<resp>\d+)/$', templatesmsj),
    url(r'^perfil/templatemsg/_(?P<resp>\d+)/$', templatesmsj),
    url(r'^perfil/templatemsg/edit/(?P<id>\d+)/$', edit_templatemsg),
    url(r'^perfil/templatemsg/edit/(?P<id>\d+)/(?P<entrar_como>\d+)/$', edit_templatemsg),
    url(r'^perfil/templatemsg/agregar/(?P<id>\d+)/(?P<entrar_como>\d+)/$', agregar_templatemsg),
    url(r'^perfil/templatemsg/agregar/(?P<id>\d+)/$', agregar_templatemsg),
    url(r'^perfil/templatemsg/agregar/_(?P<entrar_como>\d+)/$', agregar_templatemsg),
    url(r'^perfil/templatemsg/agregar/$', agregar_templatemsg),
    url(r'^perfil/templatemsg/delete/(?P<id>\d+)/(?P<entrar_como>\d+)/$', delete_templatemsg),
    url(r'^perfil/templatemsg/delete/(?P<id>\d+)/$', delete_templatemsg),

    url(r'^perfil/blacklist/$', lista_negra),
    url(r'^perfil/blacklist/(?P<entrar_como>\d+)/$', lista_negra),
    url(r'^perfil/blacklist/(?P<entrar_como>\d+)/_(?P<resp>\d+)/$', lista_negra),
    url(r'^perfil/blacklist/_(?P<resp>\d+)/$', lista_negra),
    url(r'^perfil/blacklist/edit/(?P<id>\d+)/$', edit_blacklist),
    url(r'^perfil/blacklist/edit/(?P<id>\d+)/(?P<entrar_como>\d+)/$', edit_blacklist),
    url(r'^perfil/blacklist/agregar/(?P<entrar_como>\d+)/$', agregar_blacklist),
    url(r'^perfil/blacklist/agregar/$', agregar_blacklist),
    url(r'^perfil/blacklist/delete/(?P<id>\d+)/(?P<entrar_como>\d+)/$', delete_blacklist),
    url(r'^perfil/blacklist/delete/(?P<id>\d+)/$', delete_blacklist),

    url(r'^perfil/estadoenvio/$', estadenv),
    url(r'^perfil/estadoenvio/(?P<entrar_como>\d+)/$', estadenv),
    url(r'^perfil/estadoenvio/(?P<entrar_como>\d+)/_(?P<resp>\d+)/$', estadenv),
    url(r'^perfil/estadoenvio/_(?P<resp>\d+)/$', estadenv),
    url(r'^perfil/estadoenvio/edit/(?P<id>\d+)/$', edit_estadenv),
    url(r'^perfil/estadoenvio/edit/(?P<id>\d+)/(?P<entrar_como>\d+)/$', edit_estadenv),
    url(r'^perfil/estadoenvio/agregar/(?P<entrar_como>\d+)/$', agregar_estadenv),
    url(r'^perfil/estadoenvio/agregar/$', agregar_estadenv),
    url(r'^perfil/estadoenvio/delete/(?P<id>\d+)/(?P<entrar_como>\d+)/$', delete_estadenv),
    url(r'^perfil/estadoenvio/delete/(?P<id>\d+)/$', delete_estadenv),

    url(r'^perfil/bug/$', listar_bug),
    url(r'^perfil/bug/_(?P<resp>\d+)/$', listar_bug),
    url(r'^perfil/bug/_(?P<resp>\d+)/(?P<entrar_como>\d+)/$', listar_bug),

    url(r'^perfil/bug/(?P<entrar_como>\d+)/$', listar_bug),
    url(r'^perfil/bug/agregar/$', agregar_bug),
    url(r'^perfil/bug/agregar/(?P<entrar_como>\d+)/$', agregar_bug),

    url(r'^perfil/bug/responder/(?P<id>\d+)/$', responder_bug),
    url(r'^perfil/bug/responder/(?P<id>\d+)/(?P<entrar_como>\d+)/$', responder_bug),

    url(r'^perfil/bug/delete/(?P<id>\d+)/$', delete_bug),
    url(r'^perfil/bug/delete/(?P<id>\d+)/(?P<entrar_como>\d+)/$', delete_bug),

    url(r'^perfil/bug/ver/(?P<id>\d+)/(?P<entrar_como>\d+)/$', ver_bug),
    url(r'^perfil/bug/ver/(?P<id>\d+)/$', ver_bug),

    url(r'^perfil/bug2/ver/(?P<id>\d+)/(?P<entrar_como>\d+)/$', ver_bug2),
    url(r'^perfil/bug2/ver/(?P<id>\d+)/$', ver_bug2),

    url(r'^perfil/bug2/agregar/$', agregar_bug2),
    url(r'^perfil/bug2/agregar/(?P<entrar_como>\d+)/$', agregar_bug2),

    url(r'^perfil/bug2/responder/(?P<id>\d+)/$', responder_bug2),
    url(r'^perfil/bug2/responder/(?P<id>\d+)/(?P<entrar_como>\d+)/$', responder_bug2),



    url(r'^perfil/mail/$', mail),
    url(r'^perfil/mail/(?P<entrar_como>\d+)/$', mail),
    url(r'^perfil/mail/(?P<entrar_como>\d+)/_(?P<response>\d+)/$', mail),
    url(r'^perfil/mail/_(?P<response>\d+)/$', mail),


    url(r'^perfil/sms/$', sms_enviar),
    url(r'^perfil/sms/(?P<entrar_como>\d+)/$', sms_enviar),
    url(r'^perfil/sms/(?P<entrar_como>\d+)/_(?P<response>\d+)/$', sms_enviar),
    url(r'^perfil/sms/_(?P<response>\d+)/$', sms_enviar),


    url(r'^perfil/(?P<tipo>\w+)/predefinido/$', predefinido),
    url(r'^perfil/(?P<tipo>\w+)/predefinido/_(?P<resp>\d+)/$', predefinido),
    url(r'^perfil/(?P<tipo>\w+)/predefinido/(?P<entrar_como>\d+)/_(?P<resp>\d+)/$', predefinido),

    url(r'^perfil/(?P<tipo>\w+)/delete/(?P<id>\d+)/$', delete_msj),
    url(r'^perfil/(?P<tipo>\w+)/edit/(?P<id>\d+)/$', edit_msj),
    url(r'^perfil/(?P<tipo>\w+)/programados/(?P<entrar_como>\d+)/$', programado),
    url(r'^perfil/(?P<tipo>\w+)/programados/$', programado),
    url(r'^perfil/(?P<tipo>\w+)/historico/$', historico),
    url(r'^perfil/(?P<tipo>\w+)/predefinido/excel/$', pred_excel),
    url(r'^perfil/(?P<tipo>\w+)/programados/excel/$', progr_excel),
    url(r'^perfil/(?P<tipo>\w+)/predefinido/agregar/$', agregar_msj),
    url(r'^perfil/(?P<tipo>\w+)/delete_envio/(?P<id>\d+)/$', delete_envio),
    url(r'^perfil/(?P<tipo>\w+)/reporte_historico/(?P<id>\d+)/$', reporte_historico),
    

    url(r'^perfil/(?P<tipo>\w+)/predefinido/(?P<entrar_como>\d+)/$', predefinido),
    url(r'^perfil/(?P<tipo>\w+)/programados/(?P<entrar_como>\d+)/(?P<responses>\d+)/$', programado),
    url(r'^perfil/(?P<tipo>\w+)/historico/(?P<entrar_como>\d+)/$', historico),
    url(r'^perfil/(?P<tipo>\w+)/historico/excel/(?P<entrar_como>\d+)/$', excel_historico),
    url(r'^perfil/(?P<tipo>\w+)/historico/excel/$', excel_historico),

    url(r'^perfil/(?P<tipo>\w+)/detalle/(?P<id>\d+)/(?P<entrar_como>\d+)/$', detalle_historico),
    url(r'^perfil/(?P<tipo>\w+)/detalle/(?P<id>\d+)/$', detalle_historico),
    
    url(r'^perfil/(?P<tipo>\w+)/detalle/(?P<id>\d+)/(?P<resp>\d+)/$', detalle_historico),
    url(r'^perfil/(?P<tipo>\w+)/detalle/(?P<id>\d+)/(?P<entrar_como>\d+)/(?P<resp>\d+)/$', detalle_historico),
    
    url(r'^perfil/(?P<tipo>\w+)/predefinido/excel/(?P<entrar_como>\d+)/$', pred_excel),
    url(r'^perfil/(?P<tipo>\w+)/programados/excel/(?P<entrar_como>\d+)/$', progr_excel),
    url(r'^perfil/(?P<tipo>\w+)/predefinido/agregar/(?P<entrar_como>\d+)/$', agregar_msj),
    url(r'^perfil/(?P<tipo>\w+)/delete_envio/(?P<id>\d+)/(?P<entrar_como>\d+)/$', delete_envio),
    url(r'^perfil/(?P<tipo>\w+)/edit/(?P<id>\d+)/(?P<entrar_como>\d+)/$', edit_msj),
    url(r'^perfil/(?P<tipo>\w+)/delete/(?P<id>\d+)/(?P<entrar_como>\d+)/$', delete_msj),
    url(r'^perfil/(?P<tipo>\w+)/reporte_historico/(?P<id>\d+)/(?P<entrar_como>\d+)/$', reporte_historico),


    url(r'^perfil/mail/desinscrito/(?P<entrar_como>\d+)/$', desinscrito),
    url(r'^perfil/mail/desinscrito/$', desinscrito),
    
    url(r'^perfil/sms/recibidos/(?P<entrar_como>\d+)/$', recibidos),
    url(r'^perfil/sms/recibidos/$', recibidos),
    
    url(r'^perfil/get_contacto/$', get_contacto),
    url(r'^perfil/get_uploads/$', get_upload),

    url(r'^perfil/ref_estad/(?P<id>\d+)/$', ref_estad),
    url(r'^trigger/ref_estad/(?P<id>\d+)/(?P<fech>\d+)/$', ref_estad_masivo),

    url(r'^login/$', login, {'template_name': 'login.html'}),
    url(r'^connecta/$', login, {'template_name': 'login.html'}),


    url(r'^logout/$', logout, {'next_page': '/login/'}),

    #estos son para el api

    url(r'^api/send_email/$', send_email),
    url(r'^api/send_sms/$', send_sms),
    url(r'^api/send_sms_snd/$', send_sms_snd),
    url(r'^api/get_status_email_sent/$', get_status_msj_sent),
    url(r'^api/get_status_sms_sent/$', get_status_msj_sent),
    url(r'^api/get_credits/$', get_credits),
    
    url(r'^confirmar_registro/$', confirmar_registro),
    url(r'^confirmar_restablecer/$', confirmar_restablecer),

    url(r'^viewmail/(?P<id_contacto>\d+)/(?P<id_envio>\d+)/$', webmail),
    url(r'^desinscribir/(?P<hash1>\w+)-(?P<hash2>\w+)-(?P<hash3>\w+)-(?P<hash4>\w+)-(?P<hash5>\w+)-(?P<hash6>\w+)/$', desinscribir),

#    url(r'^test/estad/$', estadisticas_estado),
#    url(r'^test/enviar_mail/$', enviar_mail),
#    url(r'^testear/retorno_sms/(?P<idresp>\d+)/$',testear_retorno_sms 
   
]
